from base64 import encode
from bs4 import BeautifulSoup
from requests import get
import sys, csv, psycopg2, datetime, yaml, babel.dates, os, webbrowser, ntplib, time


def search_date_insert():
    ntp_client = ntplib.NTPClient()
    response = ntp_client.request('pool.ntp.org')
    atimestamp = response.tx_time
    dt_object = datetime.datetime.fromtimestamp(atimestamp)
    now = dt_object.strftime("%Y-%m-%d %H:%M:%S")
    
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
    except:
        print('blad polaczenie')
    
    cur = con.cursor()
    cur.execute(f"INSERT INTO public.search_date(searchdate) VALUES ('{now}')")
    # print('insert zakonczony powodzeniem')
    con.commit()
    con.close()
    
def check_arguments():
    try:
        arg = sys.argv[1]
    except:
        arg = None
        
    if arg == '-d' or arg == '--delete':
        try:
            con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
        except:
            print('blad polaczenie')
        try:
            cur = con.cursor()
            cur.execute(f"DELETE FROM public.offers")
            con.commit()
            con.close()
        except:
            print('blad delete')
        sys.exit()
        
def check_status_new_old():    
    data = datetime.datetime.now()
    data = data.strftime('%Y-%m-%d')
    db = postgresSelect()

    for d in db:
        if not d[5]:
            pass
        else:
            dbdata = d[5].strftime('%Y-%m-%d')
            if dbdata < data:      
                postgresUpdateDate(d[0], 'stare')
            elif dbdata > data:
                print('###########################ustawiam znacznik na nowy')   
    
def postgresSelect():
    
    with open('config.yaml') as f:
        doc = yaml.safe_load(f)
    try:
        con = psycopg2.connect(database="szukajka", user=doc['config']['dbuser'], password=doc['config']['dbpass'], host=doc['config']['dbhost'], port=doc['config']['dbport'])
    except:
        print('blad polaczenie')
    try:
        cur = con.cursor()
        cur.execute("SELECT id, title, price, location, link, data, web FROM public.offers")
        rows = cur.fetchall()
        con.close()
        return rows
    except:
        print('blad selecta')
        
def postgresInsert(title, price, location,link, data, web):
    status = 'nowe'
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
    except:
        print('blad polaczenie')
    try:
        cur = con.cursor()
        cur.execute(f"INSERT INTO public.offers(title, price, location, link, data, web) VALUES ('{title}', {price}, '{location}','{link}', '{data}', '{web}')")
        # print('insert zakonczony powodzeniem')
        con.commit()
        con.close()
    except:
        print('blad inserta')
        
def postgresUpdateDate(id, status_text):
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")

    except:
        print('blad polaczenie')
    try:
        cur = con.cursor()
        cur.execute(f"UPDATE public.offers SET status='{status_text}' WHERE id={id}")
        con.commit()
        con.close()
    except:
        print('blad inserta')

def cleanUpDatabase():
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
    except:
        print('blad polaczenie')
    try:
        print('czyszczę bazę')
        cur = con.cursor()
        cur.execute(f"DELETE FROM public.offers")
        con.commit()
        con.close()
    except:
        print('blad delete')

def string_to_price_olx(string):
    price = float(string.replace('zł', '').replace(' ',''))
    return price

def string_to_price_otodom(string):
    nonBreakSpace = u'\xa0'
    price = str(string.replace('zł', '')).replace(nonBreakSpace,'')
    return price

def string_to_price_allegro(string):
    price = float(string.replace('zł', '').replace(' ','').replace(',00',''))
    return price

def link_to_writable_version(link):
    link_writable = link.split('#')
    return link_writable[0]

def check_new_offer(link):
    select_rows = postgresSelect()
    status = 'add'
    if not select_rows:
        status = 'add'
    else:
        for row in select_rows:
            if link in row:
                status = 'dontadd'
    return status

def run_site():
    webbrowser.open("http://127.0.0.1")
    os.system("pipenv run python web\web.py 1")
    
def run_site_dev():
    os.system("pipenv run python web/web.py > /dev/null 2>&1 & ")
    
def run_statusCHeck():
    os.system("pipenv run python statusCheck.py > /dev/null 2>&1 & ")
    
def writeLogs(log):
    with open('logs.txt', 'a', encoding='UTF-8') as f:
        f.writelines(f'{log}\n')

def writeLogsTitle(log):
    with open('logs.txt', 'a', encoding='UTF-8') as f:
        f.writelines(f'###################### {log}\n')

def clearLogs():
    with open('logs.txt', 'w', encoding='UTF-8') as f:
        f.writelines('')

def read_olx_offer():
    writeLogsTitle('olx')
    web = 'olx'
    with open('config.yaml') as f:
        doc = yaml.safe_load(f)
    for site in doc['sites']['olx']:        
        URL = site
        URL = URL.replace("filter_float_price%3Ato%5D=200000", f"filter_float_price%3Ato%5D={doc['config']['price']}").replace('D=1200', f"D={doc['config']['minSize']}")
        print('sprawdzam URL: ', URL)
        writeLogs(f'sprawdzam URL: {URL}')
        page = get(URL)
        bs = BeautifulSoup(page.content, 'html.parser')
        cancel = False
        for checkoffer in bs.find_all('div', class_='emptynew large lheight18'):
            try:
                check = checkoffer.find('h1', class_='c41 lheight24').get_text().strip()
                if check == 'Nie znaleziono':
                    cancel = True
                    # print('nie znaleziono ogłoszeń')
            except:
                print('nie udalo sie')
                writeLogs('nie udalo sie')
                # pass
        if cancel == True:
            continue
        else:
            for offer in bs.find_all('div',class_='offer-wrapper'):
                footer = offer.find('td', class_='bottom-cell')
                location = footer.find_all('span')
                i = 0
                
                for loc in location:
                    if i == 0:
                        city = loc.text
                    elif i == 1:
                        pass
                    i+=1

                data_writable = datetime.datetime.now()
                data_writable = data_writable.strftime('%Y-%m-%d %H:%M')
                title = offer.find('strong').get_text().strip()
                price = string_to_price_olx(offer.find('p', class_='price').get_text().strip())
                link = offer.find('a')
                link_writable = link_to_writable_version(link['href'])
                
                if check_new_offer(link_writable) == 'add':
                    print('dodaje ', web, title, price, city, link_writable, data_writable)
                    writeLogs(f'dodaje {web} {title} {price} {city} {link_writable} {data_writable}')
                    check_status_new_old()
                    postgresInsert(title, price, city, link_writable, data_writable, web)
                else:
                    # pass
                    writeLogs(f'odrzucam {web} {title} {price} {city} {link_writable} {data_writable}')
                    print('odrzucam ', web, title, price, city, link_writable, data_writable)

def read_otodom_offer():
    writeLogsTitle('otodom')
    web = 'otodom'
    with open('config.yaml') as f:
        doc = yaml.safe_load(f)
    i = 1
    for site in doc['sites']['otodom']:        
        URL = site
        URL = URL.replace('priceMax=200000', f"priceMax={doc['config']['price']}").replace('areaMin=1200', f"areaMin={doc['config']['minSize']}")
        writeLogs(f'sprawdzam URL: {URL}')
        print('sprawdzam URL: ', URL)
        page = get(URL)
        bs = BeautifulSoup(page.content, 'html.parser')
        if bs.find_all('li', class_='css-p74l73 es62z2j26') is None:
            print('nie działa css')
            writeLogs('nie działa css')
        else:
            print('css działa')
            writeLogs('css działa')
        for offer in bs.find_all('li', class_='css-p74l73 es62z2j26'):
            try:
                title = offer.find('h3', class_='css-1rhznz4 es62z2j20').get_text().strip()
                print('sprawdzam title', title)
                price = string_to_price_otodom(offer.find('p', class_='css-1bq5zfe es62z2j16').get_text().strip())
                city = offer.find('p', class_='css-80g06k es62z2j19').get_text().strip().replace('Działka na sprzedaż: ', '').split(',')
                city_writable = city[0]
                link = offer.find('a', class_='css-1c4ocg7 es62z2j23')
                link_writable = 'https://www.otodom.pl'+link['href']
                data_writable = datetime.datetime.now()
                data_writable = data_writable.strftime('%Y-%m-%d %H:%M')
                if check_new_offer(link_writable) == 'add':
                    print(f'#{i} dodaje ', web, title, price, city_writable, link_writable, data_writable)
                    writeLogs(f'#{i} dodaje {web} {title} {price} {city_writable} {link_writable} {data_writable}')
                    check_status_new_old()
                    postgresInsert(title, price, city_writable, link_writable, data_writable, web)
                    i+=1
                else:
                    # pass
                    writeLogs(f'odrzucam {web} {title} {price} {city_writable} {link_writable} {data_writable}')
                    print('odrzucam ', web, title, price, city_writable, link_writable, data_writable)
            except:
                print('nie działają css')
                writeLogs('nie działają css')
                        
def read_adresowo_offer():
    writeLogsTitle('adresowo')
    web = 'adresowo'
    with open('config.yaml') as f:
        doc = yaml.safe_load(f)
    for site in doc['sites']['adresowo']:        
        URL = site
        URL = URL.replace('priceMax=200000', f"priceMax={doc['config']['price']}")
        writeLogs(f'sprawdzam URL: {URL}')
        print('sprawdzam URL: ', URL)
        page = get(URL)
        bs = BeautifulSoup(page.content, 'html.parser')
        for offer in bs.find_all('section', class_='search-results__item'):
            title = offer.find('strong').get_text().strip()
            price = string_to_price_olx(offer.find('div', class_='result-info__price result-info__price--total').get_text().strip())
            city = offer.find('strong').get_text().strip()
            link = offer.find('a')
            link_writable = 'https://adresowo.pl'+link['href']
            data_writable = datetime.datetime.now()
            data_writable = data_writable.strftime('%Y-%m-%d %H:%M')
            if check_new_offer(link_writable) == 'add':
                print('dodaje ', web, title, price, city, link_writable, data_writable)
                writeLogs(f'dodaje {web} {title} {price} {city} {link_writable} {data_writable}')
                check_status_new_old()
                postgresInsert(title, price, city, link_writable, data_writable, web)
            else:
                writeLogs(f'odrzucam {web} {title} {price} {city} {link_writable} {data_writable}')
                print('odrzucam ', web, title, price, city, link_writable, data_writable)

def read_allegro_offer():
    web = 'allegro'
    with open('config.yaml') as f:
        doc = yaml.safe_load(f)
    i = 1
    for site in doc['sites']['allegro']:        
        URL = site
        print('sprawdzam URL: ', URL)
        page = get(URL)
        pagesTable = []
        pagesTable.append(URL)
        bs = BeautifulSoup(page.content, 'html.parser')
        pages = bs.find('div', class_='opbox-sheet _26e29_11PCu card _9f0v0 msts_n7')
        print('#################', pages)
        pagesNumber = int(pages.replace('następna', '').split('z')[1])
        # pagesNumber = int(3)
        print('liczba stron',pagesNumber)
        
        if pagesNumber > 1:
            for x in range(1, pagesNumber):
                print(x+1)
                pagesTable.append(URL+f'&p={x+1}')
                
        for page in pagesTable:
            URL = page
            URL = URL.replace('priceMax=200000', f"priceMax={doc['config']['price']}")
            print('sprawdzam URL: ', URL)
            page = get(URL)
            bs = BeautifulSoup(page.content, 'html.parser')
            
            for offer in bs.find_all('div', class_='opbox-listing'):
                for of in offer.find_all('article', class_='mx7m_1 mnyp_co mlkp_ag _9c44d_19pGn'):
                    title = of.find('h2', class_='mgn2_14 m9qz_yp mqu1_16 mp4t_0 m3h2_0 mryx_0 munh_0').get_text()
                    titleTMP = of.find('h2', class_='mgn2_14 m9qz_yp mqu1_16 mp4t_0 m3h2_0 mryx_0 munh_0').get_text().strip().split(',')
                    price = string_to_price_allegro(of.find('div', class_='msa3_z4 _9c44d_2K6FN').get_text().strip())
                    city = titleTMP[1]
                    link = of.find('a', class_='msts_9u mg9e_0 mvrt_0 mj7a_0 mh36_0 mpof_ki m389_6m mx4z_6m m7f5_6m mse2_k4 m7er_k4 _9c44d_1ILhl')
                    link_writable = link['href']
                    data_writable = datetime.datetime.now()
                    data_writable = data_writable.strftime('%Y-%m-%d %H:%M')
                    i+=1
                    print(title, link_writable)
                    
                    if check_new_offer(link_writable) == 'add':
                        print('dodaje ', web, title, price, city, link_writable, data_writable)
                        check_status_new_old()
                    else:
                        pass
                        print('odrzucam ', web, title, price, city, link_writable, data_writable)
                        
def read_morizon_offer():
    writeLogsTitle('morizon')
    web = 'morizon'
    with open('config.yaml') as f:
        doc = yaml.safe_load(f)
    i = 1
    
    for site in doc['sites']['morizon']:        
        URL = site
        writeLogs(f'sprawdzam URL: {URL}')
        print('sprawdzam URL: ', URL)
        page = get(URL)
        pagesTable = []
        pagesTable.append(URL)
        bs = BeautifulSoup(page.content, 'html.parser')
        pages = bs.find('div', class_='mz-card__item mz-card__item--pagination')
        strona = pages.get_text().strip().split('\n')
        strona_clean = [x for x in strona if x != '']
        pagesNumber = len(strona_clean)
        print('liczba stron',pagesNumber)
        writeLogs(f'liczba stron {pagesNumber}')
        
        if pagesNumber > 1:
            for x in range(1, pagesNumber):
                print(x+1)
                pagesTable.append(URL+f'&page={x+1}')
                
        print(pagesTable)
        writeLogs(f'pagesTable {pagesTable}')
        
        for page in pagesTable:
            URL = page
            writeLogs(f'sprawdzam URL: {URL}')
            print('sprawdzam URL: ', URL)
            page = get(URL)
            bs = BeautifulSoup(page.content, 'html.parser')
            
            for offer in bs.find_all('section', class_='single-result__content single-result__content--height'):
                check_add = offer.find('p', class_='single-result__category')
                
                if check_add == None:
                    print('pomijam reklame')
                    writeLogs('pomijam reklame')
                else:
                    if 'Działka na sprze' in check_add.get_text():  
                        try:
                            title = offer.find('h3', class_='single-result__category single-result__category--title').get_text().strip()   
                        except:
                            title = 'brak tytułu'           
                        city = offer.find('h2', class_='single-result__title').get_text()
                        price = string_to_price_otodom(offer.find('p', class_='single-result__price').get_text().strip())
                        link = offer.find('a', class_='property_link property-url')
                        link_writable = link['href']
                        data_writable = datetime.datetime.now()
                        data_writable = data_writable.strftime('%Y-%m-%d %H:%M')
                        
                        if check_new_offer(link_writable) == 'add':
                            print('dodaje ', web, title, price, city, link_writable, data_writable)
                            writeLogs(f'dodaje {web} {title} {price} {city} {link_writable} {data_writable}')
                            check_status_new_old()
                            postgresInsert(title, price, city, link_writable, data_writable, web)
                        else:
                            writeLogs(f'odrzucam {web} {title} {price} {city} {link_writable} {data_writable}')
                            print('odrzucam ', web, title, price, city, link_writable, data_writable)
                

if __name__ == '__main__':
    try:
        arg = sys.argv[1]
    except:
        arg = None
        print('brak argumentu')
        check_arguments()
        clearLogs()
        read_olx_offer()
        read_otodom_offer()
        read_adresowo_offer()
        search_date_insert()
        read_morizon_offer()
        run_statusCHeck()
        # run_site()
        
    if arg == '-c' or arg == '--cleanup':
        cleanUpDatabase()

    elif arg == '-s' or arg == '--site':
        check_arguments()
        read_olx_offer()
        read_otodom_offer()
        read_adresowo_offer()
        read_morizon_offer()
        # read_allegro_offer()
        run_site()
    elif arg == '-f' or arg == '--full':
        check_arguments()
        read_olx_offer()
        read_otodom_offer()
        read_adresowo_offer()
        read_morizon_offer()
        search_date_insert()
        # read_allegro_offer()
        # run_site_dev()
    elif arg == '-os' or arg == '--onlysite':
        run_site()
    elif arg == '-dev' or arg == '--develop':
        check_arguments()
        read_olx_offer()
        read_otodom_offer()
        read_adresowo_offer()
        search_date_insert()
        read_morizon_offer()
        run_site_dev()
        run_statusCHeck()
    elif arg == '-t' or arg == '--test':
        check_arguments()
        clearLogs()
        # read_olx_offer()
        read_otodom_offer()
        # read_adresowo_offer()
        # read_morizon_offer()
        run_statusCHeck()
        # search_date_insert()
        # run_site_dev()
    elif arg == '-g' or arg == '--goodmode':
        check_arguments()
        read_olx_offer()
        read_otodom_offer()
        read_adresowo_offer()
        read_morizon_offer()
        search_date_insert()
        # read_allegro_offer()
        # run_site_dev()
    elif arg == '-otodom':
        check_arguments()
        # read_olx_offer()
        read_otodom_offer()
        # read_adresowo_offer()
        # read_morizon_offer()
        # search_date_insert()
        # read_allegro_offer()
        # run_site_dev()
    
    #! poprawić wczytywanie link w allegro
