from bs4 import BeautifulSoup
from requests import get
import sys, csv, psycopg2, datetime, yaml, babel.dates, os, webbrowser, ntplib, time, os
def writeLogs(log):
    with open('logs.txt', 'a', encoding='UTF-8') as f:
        f.writelines(f'{log}\n')
def writeLogsTitle(log):
    with open('logs.txt', 'a', encoding='UTF-8') as f:
        f.writelines(f'@@@@@@@@@@@@@@@@@@@ {log}\n')
def postgresUpdate(link, status_text):
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
        # print('connection established')
    except:
        print('blad polaczenia')
    try:
        cur = con.cursor()
        cur.execute(f"UPDATE public.offers SET status='{status_text}' WHERE link='{link}'")
        con.commit()
        con.close()
    except:
        print('blad inserta')

def cleanUpDatabase():
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
        # print('connection established')
    except:
        print('blad polaczenie')
    try:
        print('czyszczę bazę')
        cur = con.cursor()
        cur.execute(f"DELETE FROM public.offers")
        con.commit()
        con.close()
    except:
        print('blad delete')

def string_to_price_olx(string):
    price = float(string.replace('zł', '').replace(' ',''))
    return price
def string_to_price_otodom(string):
    nonBreakSpace = u'\xa0'
    price = str(string.replace('zł', '')).replace(nonBreakSpace,'')
    return price

def string_to_price_allegro(string):
    price = float(string.replace('zł', '').replace(' ','').replace(',00',''))
    return price

def link_to_writable_version(link):
    link_writable = link.split('#')
    return link_writable[0]



def postgresSelect():
    with open('config.yaml') as f:
        doc = yaml.safe_load(f)
    try:
        con = psycopg2.connect(database="szukajka", user=doc['config']['dbuser'], password=doc['config']['dbpass'], host=doc['config']['dbhost'], port=doc['config']['dbport'])
    except:
        print('blad polaczenie')
    try:
        cur = con.cursor()
        cur.execute("SELECT id, link, status FROM public.offers where status='nowe' or status='stare' ORDER BY id DESC")
        # cur.execute("SELECT id, link, status FROM public.offers where link = 'https://www.morizon.pl/oferta/sprzedaz-dzialka-warszawski-zachodni-blonie-1099m2-mzn2033682030' or link = 'https://www.morizon.pl/oferta/sprzedaz-dzialka-warszawski-zachodni-blonie-mkwadrat-eu-lokalni-eksperci-nieruchomosci-1000m2-mzn2039536597' or link = 'https://www.morizon.pl/oferta/sprzedaz-dzialka-warszawski-zachodni-blonie-14000m2-mzn2033250510' or link = 'https://www.otodom.pl/pl/oferta/dzialka-bud-1390-m2-za-135tys-lub-3603-m2-za-235ty-ID4eO8R' or link = 'https://www.otodom.pl/pl/oferta/dzialka-budowlana-w-bloniu-ID4dBHg' ORDER BY id")
        # cur.execute("SELECT id, link, status FROM public.offers where  link = 'https://www.otodom.pl/pl/oferta/wszystkie-media-kanal-w-dzial-3-5km-centrum-ID4evJG' or link = 'https://www.otodom.pl/pl/oferta/dzialka-1-773-m-kampinos-ID4eZ24' ORDER BY id")
        rows = cur.fetchall()
        con.close()
        return rows
    except:
        print('blad selecta')

def statusCheck(links_list):
    writeLogsTitle('statusCheck')
    morizonText = ' jest już nieaktualne'
    morizonText2 = 'Działki powiat warszawski zachodni - oferty sprzedaży'
    otodomText = 'To ogłoszenie nie jest juz dostępne'
    how = len(links_list)
    i = 1
    wygasly = 0
    for l in links_list:
        st = ''.join(l[1])
        URL = st
        
        # print(f'{i} z {len(links_list)} sprawdzam URL: {l[0]} {URL} ma status {l[2]}')
        page = get(URL)
        bs = BeautifulSoup(page.content, 'html.parser')
        # bs2 = BeautifulSoup(page.content)
        with open('statusCheck.txt', 'w', encoding='UTF-8') as f:
            f.write(str(bs))
        if page.url != URL:
            if l[2] == 'wygasło':
                print(f'{l[0]} {i}/{len(links_list)} wykryto niezgodność adresu - wygasło {URL}')
                writeLogs(f'{l[0]} {i}/{len(links_list)} wykryto niezgodność adresu - wygasło {URL}')
            else:
                print(f'{l[0]} {i}/{len(links_list)} wykryto niezgodność adresu --UPDATE-- wygasło {URL}')
                writeLogs(f'{l[0]} {i}/{len(links_list)} wykryto niezgodność adresu --UPDATE-- wygasło {URL}')
                postgresUpdate(URL, 'wygasło')
                wygasly += 1  
        else:
            if open('statusCheck.txt', 'r', encoding='UTF-8').read().find(morizonText2) > -1 or open('statusCheck.txt', 'r', encoding='UTF-8').read().find(morizonText) > -1:
                if l[2] == 'wygasło':
                    print(f'{l[0]} {i}/{len(links_list)} wygasło {URL}')
                    writeLogs(f'{l[0]} {i}/{len(links_list)} wygasło {URL}')
                else:
                    print(f'{l[0]} --UPDATE-- {i}/{len(links_list)} wygasło {URL}')
                    writeLogs(f'{l[0]} --UPDATE-- {i}/{len(links_list)} wygasło {URL}')
                    postgresUpdate(URL, 'wygasło')
                    wygasly += 1    
            else:
                print(f'{l[0]} {i}/{len(links_list)} aktywne {URL}')
                writeLogs(f'{l[0]} {i}/{len(links_list)} aktywne {URL}')
        # with open('statusCheck.txt', 'r', encoding='UTF-8') as f:
        #     # if 'Działki powiat warszawski zachodni - oferty sprzedaży'in f.read() or 'Działki powiat warszawski zachodni - oferty sprzedaży'in f.read() :
        #     readFile = f.read()
        #     if morizonText in readFile or morizonText2 in readFile or otodomText in readFile:
        #         print('oferta nieaktualna')
        #         if l[2] == 'wygasłe':
        #             print('olewam bo już w bazie zmienione na wygasałe')
        #         else:
        #             print('ustawiam na wygasłe w bazie danych')
        #             wygasly += 1
                    
        #             # postgresUpdate(URL, 'wygasłe')
        #     else:
        #         print('oferta aktualna')
        i += 1
    print(f'wygasło {wygasly}')
        # if pages is None:
        #     print('aktywne')
        # elif pages is not None:
        #     print('nieaktywne')
        #     print(pages)
    
    


if __name__ == '__main__':
    statusCheck(postgresSelect())
    
