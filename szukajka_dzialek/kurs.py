from bs4 import BeautifulSoup
from requests import get


URL = 'https://www.olx.pl/nieruchomosci/dzialki/sprzedaz/blonie_78439/?search%5Bfilter_enum_type%5D%5B0%5D=dzialki-budowlane&search%5Bfilter_float_price%3Ato%5D=200000&search%5Bfilter_float_m%3Afrom%5D=1200&search%5Bdist%5D=5'
page = get(URL)

bs = BeautifulSoup(page.content, 'html.parser')

for offer in bs.find_all('div',class_='offer-wrapper'):
    footer = offer.find('td', class_='bottom-cell')
    location = footer.find('small', class_='breadcrumb').get_text().strip()
    title = offer.find('strong').get_text().strip()
    price = offer.find('p', class_='price').get_text().strip()
    link = offer.find('a')
    print(f"{title} | {price} | {location} | {link['href']}")
    # break