$(function() {
    var $cells = $("td");
    
    $("#search").keyup(function() {
        var val = $.trim(this.value).toUpperCase();
        if (val === "")
        $cells.parent().show();
        else {
            $cells.parent().hide();
            $cells.filter(function() {
                return -1 != $(this).text().toUpperCase().indexOf(val);
            }).parent().show();
        }
    });
});

$(function() {
    var $cells = $("td.tdprice");

    $("#tdsearch").keyup(function() {
        var val = $.trim(this.value).toUpperCase();
        console.log($(this).text().toUpperCase());
        if (val === "")
            $cells.parent().show();
        else {
            $cells.parent().hide();
            $cells.filter(function() {
                return -1 != $(this).text().toUpperCase().indexOf(val) || $( this ).text().replace(' ZŁ', '') < val;
            }).parent().show();
        }
    });
});

function openNewBackgroundTab(){
    var a = document.createElement("a");
    a.href = "http://www.google.com/";
    var evt = document.createEvent("MouseEvents");
    evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0,
                                true, false, false, false, 0, null);
    a.dispatchEvent(evt);
}