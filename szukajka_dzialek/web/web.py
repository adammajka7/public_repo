from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
import psycopg2, os

app = Flask(__name__, static_folder='static/')
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:secret@psip:30200/szukajka'
db = SQLAlchemy(app)

querySelectLike = "SELECT title, price, location,link, id, web, data, status, moj_status FROM public.offers WHERE moj_status='+' ORDER BY moj_status, status, web DESC, data DESC, location, price DESC "
querySelectNotLike = "SELECT title, price, location,link, id, web, data, status, moj_status FROM public.offers WHERE moj_status='-' ORDER BY moj_status, status, web DESC, data DESC, location, price DESC "
querySelectNew = "SELECT title, price, location,link, id, web, data, status, moj_status FROM public.offers WHERE status='nowe' AND moj_status IN ('bez statusu','+') ORDER BY status, web DESC, data DESC, location, price DESC "
querySelectExpired = "SELECT title, price, location,link, id, web, data, status, moj_status FROM public.offers WHERE status='wygasło' ORDER BY status, web DESC, data DESC, location, price DESC "
querySelectOld = "SELECT title, price, location,link, id, web, data, status, moj_status FROM public.offers WHERE status='stare' AND moj_status IN ('bez statusu','+') ORDER BY status, moj_status DESC, web DESC, data DESC,location, price DESC "
querySearchDate = "SELECT searchdate FROM public.search_date Order by searchdate DESC limit 1;"
def postgresUpdate(query):
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
        # print('connection established')
    except:
        print('blad polaczenie')
    try:
        cur = con.cursor()
        cur.execute(query)
        con.commit()
        con.close()
    except:
        print('blad selecta')
        
def postgresSelectLike():
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
        # print('connection established')
    except:
        print('blad polaczenie')
    try:
        cur = con.cursor()
        cur.execute(querySelectLike)
        rows = cur.fetchall()
        con.close()
        return rows
    except:
        print('blad selecta')
def postgresSelectNotLike():
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
        # print('connection established')
    except:
        print('blad polaczenie')
    try:
        cur = con.cursor()
        cur.execute(querySelectNotLike)
        rows = cur.fetchall()
        con.close()
        return rows
    except:
        print('blad selecta')
        
def checkVersion():
    with open('version', 'r') as f:
        version = f.read() 
    return version
def postgresSelectNew():
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
        # print('connection established')
    except:
        print('blad polaczenie')
    try:
        cur = con.cursor()
        cur.execute(querySelectNew)
        rows = cur.fetchall()
        con.close()
        return rows
    except:
        print('blad selecta')

def postgresSelectOld():
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
        # print('connection established')
    except:
        print('blad polaczenie')
    try:
        cur = con.cursor()
        cur.execute(querySelectOld)
        rows = cur.fetchall()
        con.close()
        return rows
    except:
        print('blad selecta')
        
def postgresSelectExpired():
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
        # print('connection established')
    except:
        print('blad polaczenie')
    try:
        cur = con.cursor()
        cur.execute(querySelectExpired)
        rows = cur.fetchall()
        con.close()
        return rows
    except:
        print('blad selecta')
def postgresSearchDate():
    try:
        con = psycopg2.connect(database="szukajka", user="postgres", password="secret", host="psip", port="30200")
        # print('connection established')
    except:
        print('blad polaczenie')
    try:
        cur = con.cursor()
        cur.execute(querySearchDate)
        rows = cur.fetchall()
        con.close()
        return rows
    except:
        print('blad selecta')

        
@app.route('/')
def index():
    try:
        offers_new = postgresSelectNew()
    except:
        pass
    try:
        offers_old = postgresSelectOld()
    except:
        pass
    try:
        search_date = postgresSearchDate()
    except:
        pass
    version = checkVersion()
    
    return render_template('index.html', v1=offers_new, v2=offers_old, sd = search_date[0][0], v3 = version)
@app.route('/nauka')
def nauka():
    return render_template('nauka.html')

@app.route('/iframe', methods=['POST','GET'])
def iframe():
    web = request.form.get('web')
    offerid = request.form.get('id')
    print(web)
    return render_template('offersFrame.html', iframe=web, oid=offerid)

@app.route('/', methods=['POST'])
def send():
    try:
        offers_new = postgresSelectNew()
    except:
        pass
    try:
        offers_old = postgresSelectOld()
    except:
        pass
    try:
        search_date = postgresSearchDate()
    except:
        pass
    version = checkVersion()
    os.system("pipenv run python szukajka.py")
    return render_template('index.html', v1=offers_new, v2=offers_old, sd=search_date[0][0], v3=version)

@app.route('/wszystkie', methods=['POST'])
def wszystkie():
    like_or_not = request.form.get("likeornot")
    offer_id = request.form.get("id")
    print(like_or_not, offer_id)
    queryUpdate = f"UPDATE public.offers SET moj_status='{like_or_not}' where id={offer_id}"
    postgresUpdate(queryUpdate)
    try:
        offers_new = postgresSelectNew()
    except:
        pass
    try:
        offers_old = postgresSelectOld()
    except:
        pass
    try:
        search_date = postgresSearchDate()
    except:
        pass
    version = checkVersion()
    # os.system("pipenv run python szukajka.py")
    return render_template('index.html', v1=offers_new, v2=offers_old, sd=search_date[0][0], v3=version)

@app.route('/lubie', methods=['POST'])
def lubie():
    like_or_not = request.form.get("likeornot")
    offer_id = request.form.get("id")
    print(like_or_not, offer_id)
    queryUpdate = f"UPDATE public.offers SET moj_status='{like_or_not}' where id={offer_id}"
    postgresUpdate(queryUpdate)
    try:
        offers_new = postgresSelectLike()
    except:
        pass
    try:
        offers_old = postgresSelectNotLike()
    except:
        pass
    try:
        search_date = postgresSearchDate()
    except:
        pass
    version = checkVersion()
    # os.system("pipenv run python szukajka.py")
    return render_template('index.html', v1=offers_new, sd=search_date[0][0], v3=version)

@app.route('/expired', methods=['GET', 'POST'])
def expired():
    like_or_not = request.form.get("likeornot")
    offer_id = request.form.get("id")
    print(like_or_not, offer_id)
    queryUpdate = f"UPDATE public.offers SET moj_status='{like_or_not}' where id={offer_id}"
    postgresUpdate(queryUpdate)
    try:
        offers_expired = postgresSelectExpired()
    except:
        pass
    version = checkVersion()
    # os.system("pipenv run python szukajka.py")
    return render_template('index.html', v1=offers_expired, v3=version)
@app.route('/logs', methods=['GET', 'POST'])
def logs():
    with open('logs.txt', 'r', encoding='utf-8') as f:
        logs = f.readlines()
        print(logs)
    
    version = checkVersion()
    # os.system("pipenv run python szukajka.py")
    return render_template('logs.html', v3=version, logs=logs)


@app.route('/nielubie', methods=['POST'])
def nie_lubie():
    
    like_or_not = request.form.get("likeornot")
    offer_id = request.form.get("id")
    print(like_or_not, offer_id)
    queryUpdate = f"UPDATE public.offers SET moj_status='{like_or_not}' where id={offer_id}"
    postgresUpdate(queryUpdate)
    # os.system("pipenv run python szukajka.py")
    try:
        offers_new = postgresSelectNotLike()
    except:
        pass
    try:
        offers_old = postgresSelectLike()
    except:
        pass
    try:
        search_date = postgresSearchDate()
    except:
        pass
    version = checkVersion()
    return render_template('index.html', v1=offers_new, sd=search_date[0][0], v3=version)

@app.route('/test')
def test():
    try:
        offers_new = postgresSelectNew()
    except:
        pass
    try:
        offers_old = postgresSelectOld()
    except:
        pass
    
    first_name = request.form.get("pname")
    print(first_name)
    # os.system("pipenv run python szukajka.py")
    return render_template('test.html', v1=offers_new, v2=offers_old)
@app.route('/test2')
def test2():
    version = checkVersion()
    return render_template('test2.html', v3 = version)




if __name__ == '__main__':
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(host="0.0.0.0", port=80, debug=True)