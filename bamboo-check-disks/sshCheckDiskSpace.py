import paramiko, sys, os, smtplib, base64
from datetime import datetime
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# pobieranie z variables planu bamboo
cleanJobStart = int(os.environ['bamboo_checkDisk_startTime'])
cleanJobStop = int(os.environ['bamboo_checkDisk_stopTime'])
alarm_warning = int(os.environ['bamboo_checkDisk_alarmWarning'])
alarm_alarm = int(os.environ['bamboo_checkDisk_alarmAlarm'])
print(f'config - start period {cleanJobStart} - {cleanJobStop}')
print(f'config - alarm period warning {alarm_warning}, alarm {alarm_alarm}')
SSH_ADDRESS_TABLICA = ["10.2.227.9",'10.2.227.15','10.2.227.16','10.2.227.17', '10.2.227.12']
fileName = 'data.html'
messageSubject = "Sprawdzanie zajetosci dyskow"
SSH_USERNAME = "sshuser"
SSH_KEY = str(os.environ['bamboo_checkDisk_secretSSH'])
SSH_PASSWORD = 'key'
SSH_COMMAND = "df -h /opt"
smtp_server = "smtpgate"
port = 25  
sender_email = "bamboo-noreply@mail.pl"
receiver_email = ['mail1','mail2','mail3']

sendMail = True

class CheckDisk:
    def __init__(self, sshAddres, username, password_ssh, sshcommand):
        self.sshAddres = sshAddres
        self.username = username
        self.password_ssh = password_ssh
        self.sshcommand = sshcommand

    def checkSsh(self):
        global outOfSpaceBm
        outOfSpaceBm = []
        f = open(fileName, "w")
        f.write('''
<html>
<table>
''')
        f.write('<tr style="background-color:blueviolet"><td colspan=6>###### {} #######</td></tr>\n\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        message = []
        """zbieranie danych

        Args:
            sshAddres (adres ip): tablica adresow ip
            username (uzytkownik): uzytkownik ssh
        """
        for SSH_ADDRESS in self.sshAddres:
            print(len(self.sshAddres))
            
            if SSH_ADDRESS:
                if SSH_ADDRESS == '10.2.227.15':
                        self.sshcommand = 'df -h /u01'
                if SSH_ADDRESS == '10.2.227.12':
                        self.sshcommand = 'df -h /u01 /var'
                print('SSH_ADDRESS: ', SSH_ADDRESS)
                print('if spelniony')
                print(SSH_ADDRESS)
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh_stdin = ssh_stdout = ssh_stderr = None

                try:
                    ssh.connect(SSH_ADDRESS, username=self.username, key_filename=self.password_ssh)
                    # ! rozne dyski dla roznych maszyn                     
                    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(self.sshcommand)
                except Exception as e:
                    sys.stderr.write("SSH connection error: {0}".format(e))
                
                if ssh_stdout:
                    # sys.stdout.write(str(ssh_stdout.read()))
                    data = str(ssh_stdout.read())
                    dataSplit = str(data)
            
                    if SSH_ADDRESS == '10.2.227.9':
                        SSH_ADDRESS = 'BM1 - 10.2.227.9'
                    elif SSH_ADDRESS == '10.2.227.15':
                        SSH_ADDRESS = 'BM2 - 10.2.227.15'
                    elif SSH_ADDRESS == '10.2.227.16':
                        SSH_ADDRESS = 'BM3 - 10.2.227.16'
                    elif SSH_ADDRESS == '10.2.227.17':
                        SSH_ADDRESS = 'BM4 - 10.2.227.17'
                    elif SSH_ADDRESS == '10.2.227.12':
                        SSH_ADDRESS = 'BAMBOO PROD - 10.2.227.12'
                        
                    f.write('<tr style="background-color:rgb(0, 195, 255)"><td colspan=6>'+SSH_ADDRESS+'</td></tr>'+'\n')
                    
                    for zdanie in dataSplit.split('\\n'):
                        zdanieReplace = zdanie.replace("'",'').replace("bFilesystem",'Filesystem')
                        list_z_replace = zdanieReplace.split(" ")

                        while "" in list_z_replace: 
                            list_z_replace.remove("")
                        if not list_z_replace:
                            pass
                        else:
                            print('################list_z_replace: ', list_z_replace)
                        
                            if 'Use%' in list_z_replace[4]:
                                f.write('<tr style="background-color:rgb(168, 235, 255)"><td>'+list_z_replace[0]+'</td><td>'+list_z_replace[1]+'</td><td>'+list_z_replace[2]+'</td><td>'+list_z_replace[3]+'</td><td>'+list_z_replace[4]+'</td><td>'+list_z_replace[5]+'</td></tr>'+'\n')
                            elif '%' in list_z_replace[4]:
                                liczba = list_z_replace[4]
                                liczba = liczba.replace('%','')
                                if int(liczba)>=alarm_alarm:             
                                    f.write('<tr style="background-color:rgb(168, 235, 255)"><td>'+list_z_replace[0]+'</td><td>'+list_z_replace[1]+'</td><td>'+list_z_replace[2]+'</td><td>'+list_z_replace[3]+'</td><td style="background-color:red">'+list_z_replace[4]+'</td><td>'+list_z_replace[5]+'</td></tr>'+'\n')
                                    if 'BM' in SSH_ADDRESS:
                                        outOfSpaceBm.append(SSH_ADDRESS[0:3])
                                elif int(liczba)>=alarm_warning:
                                    f.write('<tr style="background-color:rgb(168, 235, 255)"><td>'+list_z_replace[0]+'</td><td>'+list_z_replace[1]+'</td><td>'+list_z_replace[2]+'</td><td>'+list_z_replace[3]+'</td><td style="background-color:orange">'+list_z_replace[4]+'</td><td>'+list_z_replace[5]+'</td></tr>'+'\n')
                                else:
                                    f.write('<tr style="background-color:rgb(168, 235, 255)"><td>'+list_z_replace[0]+'</td><td>'+list_z_replace[1]+'</td><td>'+list_z_replace[2]+'</td><td>'+list_z_replace[3]+'</td><td style="background-color:rgb(70, 187, 70)">'+list_z_replace[4]+'</td><td>'+list_z_replace[5]+'</td></tr>'+'\n')
                            else:
                                f.write('<tr style="background-color:rgb(168, 235, 255)"><td>'+list_z_replace[0]+'</td><td>'+list_z_replace[1]+'</td><td>'+list_z_replace[2]+'</td><td>'+list_z_replace[3]+'</td><td>'+list_z_replace[4]+'</td><td>'+list_z_replace[5]+'</td></tr>'+'\n')

                if ssh_stderr:
                    sys.stderr.write(str(ssh_stderr.read()))
            elif not SSH_ADDRESS:
                print('error SSH')
        f.write('</table></html>')
        f.close()
        f = open(fileName,'r')
        message = f.read()
        print('message to ', message)
        f.close()
        os.remove(fileName)
        import webbrowser
        return message
        
class SendMail:
    def __init__(self, smtp_server, port, sender_email, receiver_email, message):
        self.smtp_server = smtp_server
        self.port = port
        self.sender_email = sender_email
        self.receiver_email = receiver_email
        self.message = message            
    def sendMail(self):
        message = self.message
        # Try to log in to server and send email
        try:
            messageM = MIMEMultipart("alternative")
            messageM["Subject"] = "Sprawdzanie zajetosci dyskow"
            server = smtplib.SMTP(self.smtp_server,self.port)
            server.ehlo()
            server.starttls() 
            # ! wysylka
            part = MIMEText(str(message), "html")
            messageM.attach(part)
            with smtplib.SMTP(self.smtp_server,self.port) as server:
                server.sendmail(self.sender_email, self.receiver_email, messageM.as_string())   
        except Exception as e:
            # Print any error messages to stdout
            print(e)
        finally:
            print('mail sended') 
            
def checkDate():
    date = datetime.now()
    date_format = date.strftime("%H")
    return int(date_format)

def keyCreate():
    base64_message = SSH_KEY
    base64_bytes = base64_message.encode('ascii')
    message_bytes = base64.b64decode(base64_bytes)
    message = message_bytes.decode('ascii')
    f = open('key', 'w')
    f.write(message)
    f.close()
    
def keyClean():
    try:
        os.remove('key') 
    except:
        print('cant remove key')   
def runClean():
    print('runCLEAN')
    print(outOfSpaceBm)
    if not outOfSpaceBm:
        print('cleaning not required')
    elif len(outOfSpaceBm) == 4:
        del outOfSpaceBm[-1]
    else:
        print(f'check time for run clean job, period is {cleanJobStart} - {cleanJobStop}')
        checkTime = checkDate()
        if checkTime >= cleanJobStart and checkTime <= cleanJobStop:
            print('its time to clean, hour is: ', checkTime)
            print(f'start cleaning for {outOfSpaceBm}')
            os.system(f'pipenv run python cleanAgents.py {outOfSpaceBm}')
        else:
            print('its not time to clean :) hour is: ', checkTime)
        
def main():
    if sendMail == False:
        keyCreate()
        f = CheckDisk(SSH_ADDRESS_TABLICA, SSH_USERNAME, SSH_PASSWORD, SSH_COMMAND)
        f.checkSsh()
        keyClean()
        
    else:
        f = CheckDisk(SSH_ADDRESS_TABLICA, SSH_USERNAME, SSH_PASSWORD, SSH_COMMAND)
        keyCreate()
        for mail in receiver_email:    
            # f.checkSsh()
            s = SendMail(smtp_server, port, sender_email, mail,f.checkSsh())
            s.sendMail()
        keyClean()
        runClean()
if __name__ == "__main__":
    main()
    