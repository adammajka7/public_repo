import requests, json, time, sys, os
from xml.etree import ElementTree

print(sys.argv)

bmdeployID = {
    'bm1':'102990086',
    'bm2':'102990085',
    'bm3':'102990084',
    'bm4':'102990066',
    'prebm1':'102990064',
    'prebm2':'102990055',
    'prebm3':'102990063',
    'prebm4':'102990065',
    }
tableBM = []
for arg in sys.argv:
    if 'cleanAgents.py' in arg:
        pass
    else:
        new = arg.replace('[', '').replace('\'', '').replace(']', '').replace(',', '').replace(' ','')
        print(new)
        tableBM.append(new.lower())
print('used hosts: ',tableBM)
class BambooAgents:
    outOfSpaceBm = tableBM
    authLogin = 'checkDisk'
    authPass = os.environ['bamboo_checkDiskSecret']
    agentsType = ['STANDARD', 'BUILDAH']
        
    def checkDeploymentStatus(self, bm):
        self.url = f'https://bamboo/rest/api/latest/deploy/environment/{bm}/results?max-results=1'
        self.headers = {'Content-Type': 'application/json'}
        self.r = requests.get(self.url, headers=self.headers, auth=(self.authLogin, self.authPass))
        self.s = self.r.text
        self.dane = json.loads(self.s)
        # print(self.dane)
        self.highestVersion_return = 0 
        for data in self.dane['results']:
            deployState = data['deploymentState']
            deployVersion = data['deploymentVersion']['items'][0]['planResultKey']['resultNumber']
            
            if deployVersion >= self.highestVersion_return:
                self.deployState_return = deployState
                self.highestVersion_return = deployVersion    
                print(f'status: {self.deployState_return}')
                print(f'build: #{self.highestVersion_return}')

        return self.deployState_return, self.highestVersion_return    
                
    def checkJobStatus(self):
        self.url = 'https://bamboo/rest/api/latest/result/ATL-BAMDEPLOY?max-results=1'
        self.headers = {'Content-Type': 'application/json'}
        self.r = requests.get(self.url, headers=self.headers, auth=(self.authLogin, self.authPass))
        root = ElementTree.fromstring(self.r.content)
        for child in root.iter('result'):
            checkJobStatus_number=child.attrib['number']
            checkJobStatus_state=child.attrib['state']
            checkJobStatus_lifeCycleState=child.attrib['lifeCycleState']
        return checkJobStatus_number, checkJobStatus_state, checkJobStatus_lifeCycleState
        
    def checkAgents(self):
        self.url = 'https://bamboo/rest/api/latest/agent?'
        self.headers = {'Content-Type': 'application/json'}
        try:
            self.r = requests.get(self.url, headers=self.headers, auth=(self.authLogin, self.authPass))
            self.s = self.r.text
            self.json_acceptable_string = self.s.replace("'", "\"")
            self.dane = json.loads(self.json_acceptable_string)
        except:
            print('error #1')
        return self.dane, self.s
    
    def fdisabling(self, id):
        self.url = 'https://bamboo/admin/agent/disableAgent.action?agentId={}'.format(id)
        self.headers = {'Content-Type': 'application/json'}
        self.r = requests.get(self.url,headers=self.headers, auth=(self.authLogin, self.authPass))
        
    def agentDisabling(self):
        agentsDisabled = []
        agentsBussy = []
        
        while True:
            for bm in self.outOfSpaceBm:
                self.agents = self.checkAgents()[0]
                for fagent in self.agents:
                    if fagent['active'] == True and bm in fagent['name']:
                        
                        if self.agentsType[0] in fagent['name'] or self.agentsType[1] in fagent['name']:
                            
                            if fagent['busy'] == True:
                                 if fagent['id'] in agentsBussy and fagent['id'] in agentsDisabled:
                                     pass
                                 elif fagent['id'] in agentsBussy and fagent['id'] not in agentsDisabled:
                                    self.fdisabling(fagent['id'])
                                    print(f"disablind agent id {fagent['id']} - name {fagent['name']}")
                                    agentsDisabled.append(fagent['id']) 
                                 elif fagent['id'] not in agentsBussy and fagent['id'] not in agentsDisabled:
                                    self.fdisabling(fagent['id'])
                                    print(f"disablind agent id {fagent['id']} - name {fagent['name']}")
                                    agentsBussy.append(fagent['id']) 
                                    agentsDisabled.append(fagent['id']) 
                                 elif fagent['id'] not in agentsBussy and fagent['id'] in agentsDisabled:
                                    agentsBussy.append(fagent['id'])  
                                    
                            elif fagent['busy'] == False:
                                if fagent['id'] in agentsBussy:
                                    agentsBussy.remove(fagent['id']) 
                                elif fagent['id'] not in agentsDisabled:
                                    self.fdisabling(fagent['id'])
                                    print(f"disablind agent id {fagent['id']} - name {fagent['name']}")
                                    agentsDisabled.append(fagent['id']) 
       
            print('agents disabled: ', agentsDisabled)
            print('agents bussy: ', agentsBussy)
            if len(agentsBussy) == 0:
                print('ready to redeploy agents')
                break
            time.sleep(10)
                
    def agentShutdown(self):
        self.checkAgents()
        for agent in self.dane:
            if agent['active'] == True and agent['busy'] == False and 'STANDARD' in agent['name'] and agent['enabled'] == False:
                self.url = 'https://bamboo/admin/agent/stopAgentNicely.action?agentId={}'.format(agent['id'])
                self.headers = {'Content-Type': 'application/json'}
                self.r = requests.get(self.url,headers=self.headers, auth=('login', 'pass'))
                print(self.r.status_code)
                print(agent)
                
    def runBuildAgents(self):
        print(f'actual build: #{self.checkJobStatus()[0]}')
        for bm in self.outOfSpaceBm:
            self.actualJobNumber = self.checkJobStatus()[0]
            self.newJobNumber = self.checkJobStatus()[0]
            self.bmUpper = bm.upper()
            self.url = f'https://bamboo/rest/api/latest/queue/ATL-BAMDEPLOY?bamboo.variable.environment=PROD&bamboo.variable.group=ALLBEZDEV&bamboo.variable.targetBM={self.bmUpper}&stage=Run On Prod'
            self.headers = {'Content-Type': 'application/json'}
            self.r = requests.post(self.url,headers=self.headers, auth=(self.authLogin, self.authPass))
            time.sleep(3)
            print('waiting for new build')
            while self.newJobNumber == self.actualJobNumber:
                self.newJobNumber = self.checkJobStatus()[0]
                time.sleep(5)
            print(f'detect new build #{self.newJobNumber}')
            print('waiting for succes deploy')
            while self.checkDeploymentStatus(bmdeployID[bm])[0] != 'SUCCESS':
                time.sleep(5)
        os.system(f'pipenv run python sshCheckDiskSpace.py')
            
        
if __name__ == '__main__':
    BambooAgents().agentDisabling()
    BambooAgents().runBuildAgents()