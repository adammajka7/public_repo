import walizkait.functions
import os

repoList = walizkait.functions.RepoList("tests/testrepos.yaml")

def test_files_count():
    files = 0
    for proj in repoList.projectNameListShow():
        srcFilesPath = os.path.join(walizkait.functions.workingPath, walizkait.functions.directory, repoList.currentDateForMnt(), proj)
        srcFiles = repoList.listFIlesInDir(srcFilesPath)
        dstFilesPath = os.path.join(walizkait.functions.mntPath, repoList.currentDateForMnt(), proj)
        dstFiles = repoList.listFIlesInDir(dstFilesPath)   
        files += len(srcFiles)
        print(f"global files number {walizkait.functions.globalFilesNumber} == dest files number{files}")
    assert walizkait.functions.globalFilesNumber == files

def test_files_hashes():
    for proj in repoList.projectNameListShow():
        srcFilesPath = os.path.join(walizkait.functions.workingPath, walizkait.functions.directory, repoList.currentDateForMnt(),proj)
        srcFiles = repoList.listFIlesInDir(srcFilesPath)
        dstFilesPath = os.path.join(walizkait.functions.mntPath, repoList.currentDateForMnt(),proj)
        dstFiles = repoList.listFIlesInDir(dstFilesPath)
        print('dstFiles: ', dstFiles)
        if dstFiles == srcFiles:
            print("number of files is correct")
            for sf in srcFiles:
                if "!success.log" in sf or "!errors.log" in sf:
                    continue
                else:
                    for df in dstFiles:
                        if sf in df:
                            file1 = repoList.hashFile(os.path.join(srcFilesPath, sf))
                            file2 = repoList.hashFile(os.path.join(dstFilesPath, df))
                            if file1 == file2:
                                print(f"source      {sf} {file1}")
                                print(f"destination {df} {file2}")
                                print(f"correct :)")
                            else:
                                print(f"source      {sf} {file1}")
                                print(f"destination {df} {file2}")
                                print(f"incorrect :(")
                            assert file1 == file2
                                
        else:
            print("number of files is incorrect")
