import walizkait.functions, os, shutil

repoList = walizkait.functions.RepoList("tests/testrepos.yaml")

def test_createFolder():
    path = "test_folder"
    isExist = os.path.exists(path)
    if not isExist:
        os.makedirs(path)
        print("The new directory is created!") 
    isExist = os.path.exists(path)
    assert isExist == True 

def test_removeFolder():
    path = "test_folder"
    isExist = os.path.exists(path)
    if isExist:
        shutil.rmtree(path)
    isExist = os.path.exists(path)
    assert isExist == False 