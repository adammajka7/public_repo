import yaml, os, shutil, requests, pytest
from datetime import datetime
from pathlib import Path
import subprocess
import glob
import shutil
import os
import hashlib
import re
import time
import sys, os, smtplib, base64
from datetime import datetime
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def dekorThreeAttempts(obj):
    def wrapper(*args, **kwargs):
        attempts = 0
        while attempts < 3:
            try:
                obj(*args, **kwargs)
                break
            except Exception as e:
                attempts += 1
                print(f"error {e}")
                print("waiting 5 sec and start again")
                time.sleep(5)
    return wrapper

class RepoList:
    global filesNumber
    global workingPath
    global directory
    global mntName
    global mntPath
    global globalFilesNumber
    globalFilesNumber = 0
    projectNameTwo = ""
    projectNamesList = []
    userFromBamboo = os.environ['bamboo_walizkaitsecretuser']
    userFromBambooSamba = os.environ['bamboo_walizkasambausersecret']
    passwordFromBamboo = os.environ['bamboo_walizkaitsecretpassword']
    passwordFromBambooSamba = os.environ['bamboo_walizkasambapasswordsecret']
    requestLimit=10000
    directory = "mnt"
    log_success = f"!success.log" 
    log_errors = f"!errors.log"
    workingPath = os.getcwd()
    mntName = "mnt"
    mntPath = os.path.join(workingPath, mntName)
    downloadedReposPath = os.path.join(workingPath, directory)
    dirPathWalizka = '//osf01186.pzu.pl/BackupPZU/BRI/Backup/Kody_zrodlowe'

    def __init__(self, repoPath):
        self.repoPath = repoPath        
    
    def showRepoPath(self):
        print(self.repoPat)
        
    def hashFile(self, filename):
        with open(filename, "rb") as f:
            bytes = f.read()
            readable_hash = hashlib.sha256(bytes).hexdigest()
        return readable_hash
        
    def getActiveBranchName(self):
        print("#####################")
        print(os.getcwd())
        print("#####################")
        head_dir = Path("../repolist/") / ".git" / "HEAD"
        with head_dir.open("r") as f: content = f.read().splitlines()

        for line in content:
            if line[0:4] == "ref:":
                return line.partition("refs/heads/")[2]        
        
    def repoListWitchBranch(self):
        repoListList = []
        
        with open(self.repoPath, "r") as stream:
            try:
                a = yaml.safe_load(stream)
                for rep in a['dane']['repoData']:
                    repoListList.append(rep)
            except yaml.YAMLError as exc:
                print(exc)
        return repoListList
    
    def createFolderIfNotExist(self):
        path = os.path.join(workingPath, directory, self.currentDateForMnt())
        isExist = os.path.exists(path)
        
        if not isExist:
            os.makedirs(path)
            print(f"creating {directory}")
            
    def deleteFolderIfExist(self):
        dirpath = os.path.join(directory)
        
        if os.path.exists(dirpath):
            shutil.rmtree(dirpath)
            print(f"cleanup {directory}")
            
    def prepareRepoNameForLogs(self, repoLink):
        repoLinkSplit = repoLink.split("?")
        if "/" in repoLinkSplit[1]:
            repoLinkSplit[1] = repoLinkSplit[1].replace("/", "%2F")
        repoLink = "".join(repoLinkSplit)
        repoLink = repoLink.replace("archiveat=ref", "archive?at=ref")
        if "release/" in repoLink:
            repoLink = repoLink.replace("release/", "release%2F")
        if "feature/" in repoLink:
            repoLink = repoLink.replace("feature/", "feature%2F")
        repoLink = repoLink.split("/")
        proj, repo, branch = repoLink[7], repoLink[9], repoLink[10]
        if "archive?format=zip" in branch:
            branch = "default-branch"
        elif "archive?at=refs%2Fheads%2F":
            branch = branch.replace("archive?at=refs%2Fheads%2F", "").replace("&format=zip", "").replace("%2F", "-")
        return f"{proj}-{repo}-{branch}" 
    
    def newestRelease(self, url):
        branches = []
        url = str(url)+"branches"
        r = requests.get(url, auth=requests.auth.HTTPBasicAuth(self.userFromBamboo, self.passwordFromBamboo), allow_redirects=True)
        dane = r.json()
        for d in dane['values']:
            if "release" in d['displayId']:
                branch = d['displayId']
                branch = branch.replace("release/", "")
                branches.append(branch)
        branchesSorted = sorted(branches, reverse=True)
        branchesNewest = sorted(branches, reverse=True)[0]
        print(f"release branches: {branchesSorted}")
        print(f"newest release branch: {branchesNewest}")
        return f"release%2F{branchesNewest}"
    
    def regex(self, url):
        #https://bitbucket.pzu.pl/projects/AD/repos/developtest/browse
        #https://bitbucket.pzu.pl/rest/api/latest/projects/AD/repos/developtest/branches
        branches = []
        url = str(url)+f"branches?limit={self.requestLimit}"
        r = requests.get(url, auth=requests.auth.HTTPBasicAuth(self.userFromBamboo, self.passwordFromBamboo), allow_redirects=True)
        dane = r.json()
        for d in dane['values']:
            branches.append(d['displayId'])
        return branches

    def prepareRepoName(self, repoLink):
        global filesNumber
        global globalFilesNumber
        filesNumber = 0
        defaultLink = 'https://bitbucket.pzu.pl/projects/CHANGEPROJECTNAME/repos/CHANGEREPONAME/browse'
        projectName = repoLink['project']['name']
        # self.projectNameTwo = repoLink['project']['name']
        # if not self.projectNameTwo in self.projectNamesList:
        #     self.projectNamesList.append(self.projectNameTwo)
        repoRegex = repoLink['project']['repoRegex']
        branchRegex = repoLink['project']['branchRegex']
        repoRegexMatch = []
        reposWebLink = []
        proApiData = f"https://bitbucket.pzu.pl/rest/api/latest/projects/{projectName}/repos?limit={self.requestLimit}"
        r = requests.get(proApiData, auth=requests.auth.HTTPBasicAuth(self.userFromBamboo, self.passwordFromBamboo), allow_redirects=True)
        dane = r.json()
        for d in dane['values']:
            for r in repoRegex:
                regMatch  = re.findall(r, d['name'])
                if regMatch:
                    repoRegexMatch.append(d['name'])
                    
        for r in repoRegexMatch:
            tmpLink = defaultLink
            reposWebLink.append(tmpLink.replace("CHANGEPROJECTNAME", projectName).replace("CHANGEREPONAME", r))
        
        preparedList = []
        if len(reposWebLink)>0:
            for repo in reposWebLink:
                repoLinkName = repo.replace("https://bitbucket.pzu.pl/projects/", "https://bitbucket.pzu.pl/rest/api/latest/projects/").replace("browse", "")        
                branches = self.regex(repoLinkName)
                for b in branches:
                    for r in branchRegex:
                        regMatch  = re.findall(r, b)
                        if regMatch:
                            print(f"Regex match {b} at {repoLinkName}")
                            filesNumber += 1
                            globalFilesNumber += 1
                            repoLinkNameReg = repoLinkName + f"archive?at=refs%2Fheads%2F{b}&format=zip"
                            preparedList.append(repoLinkNameReg)            
            return preparedList
            
    def currentDate(self):
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        return dt_string

    def currentDateForMnt(self):
        now = datetime.now()
        dt_string = now.strftime("%Y-%m-%d")
        return f"Bitbucket-{dt_string}"

    def pytest(self):
        pytest.main(["-v", "--junitxml=tests/result.xml"])
        
    @dekorThreeAttempts
    def logs(self, text, preparedUrl, number):

        path = os.path.join(workingPath, directory, self.currentDateForMnt())
        if "is not a valid ref and may not be archived" in str(text) or "does not exist" in str(text):
            print(f"blad {text}")
            with open(f'{path}/{self.log_errors}', 'a') as f:
                f.write(f'{self.currentDate()} error - {preparedUrl}\n')
        else:
            print(f"downloading {number}/{filesNumber} {preparedUrl} to {directory}")
            with open(f'{path}/{self.projectNameTwo}/{preparedUrl}.zip', 'wb') as f:
                f.write(text)
            with open(f'{path}/{self.log_success}', 'a') as f:
                f.write(f'{self.currentDate()} success - {preparedUrl}\n')
                
    @dekorThreeAttempts    
    def downloadRepos(self):
        
        path = os.path.join(workingPath, directory, self.currentDateForMnt())
        projects = self.repoListWitchBranch()
        for r in projects:
            number = 1
            projectData = r
            # TUTAJ TWORZE FOLDERY PROJEKTOW
            self.projectNameTwo = projectData['project']['name']
            if self.projectNameTwo not in self.projectNamesList:
                self.projectNamesList.append(self.projectNameTwo)
            if not os.path.isdir(os.path.join(path, self.projectNameTwo)):
                os.mkdir(os.path.join(path, self.projectNameTwo))
            # 
            preparedUrl = self.prepareRepoName(projectData)
            for p in preparedUrl:
                preparedName = self.prepareRepoNameForLogs(p)
                r = requests.get(p, auth=requests.auth.HTTPBasicAuth(self.userFromBamboo, self.passwordFromBamboo), allow_redirects=True)
                self.logs(r.content, preparedName, number)
                number += 1

    def listFIlesInDir(self, dir):
        osListDir = os.listdir(dir)
        return osListDir
    
    def osGetDirs(self):
        path = os.path.join(os.getcwd(), "downloaded_repos")
        osLIstDir = os.listdir(path)
        return osLIstDir    
    
    @dekorThreeAttempts
    def copyFolders(self, files):
        for file in files:
            path = os.path.join(os.getcwd())
            srcPath = os.path.join(path, "downloaded_repos",file)
            dstPath = os.path.join(path, "mnt", file)
            
            shutil.copytree(srcPath, dstPath, dirs_exist_ok=True)

    def checkHashFile(self):
        srcFilesPath = os.path.join(workingPath, directory, self.currentDateForMnt(), self.projectNameTwo)
        srcFiles = self.listFIlesInDir(srcFilesPath)
        dstFilesPath = os.path.join(mntPath, self.currentDateForMnt(), self.projectNameTwo)
        dstFiles = self.listFIlesInDir(dstFilesPath)
        if len(dstFiles) == len(srcFiles):
            print("number of files is correct")
            for sf in srcFiles:
                if "!success.log" in sf or "!errors.log" in sf:
                    continue
                else:
                    for df in dstFiles:
                        if sf in df:
                            file1 = self.hashFile(os.path.join(srcFilesPath, sf))
                            file2 = self.hashFile(os.path.join(dstFilesPath, df))
                            if file1 == file2:
                                print(f"source      {sf} {file1}")
                                print(f"destination {df} {file2}")
                                print(f"correct :)")
                            else:
                                print(f"source      {sf} {file1}")
                                print(f"destination {df} {file2}")
                                print(f"incorrect :(")
        else:
            print("number of files is incorrect")
    @dekorThreeAttempts
    def cleanUpBeforeDownload(self):
        rmCommand = f"sudo rm -fr mnt/*"
        subprocess.run(rmCommand, shell=True)
    @dekorThreeAttempts
    def mountShare(self):
        if not os.path.exists(mntPath):
            print("create mnt directory")
            os.mkdir("mnt")
        print("mount mnt directory")
        mountCommand = f"sudo mount -t cifs -o uid=5015,username={self.userFromBambooSamba},password={self.passwordFromBambooSamba} {self.dirPathWalizka} {mntPath}"
        subprocess.run(mountCommand, shell=True)
    def mountShareToTest(self):
        if not os.path.exists(mntPath):
            os.mkdir("mnt")        
    def umountShare(self):
        umount = f"sudo umount {mntPath}"
        subprocess.run(umount, shell=True)
    def umountShareToTest(self):
        dirpath = os.path.join('../mnt/')
        
        if os.path.exists(dirpath):
            shutil.rmtree(dirpath)
            print(f"cleanup {directory}")

    def projectNameListShow(self):
        projectList = self.projectNamesList
        return projectList
    
    def copySamba(self):
        folderName = self.currentDateForMnt()
        files = self.listFIlesInDir(mntPath)
        self.copyFolders(self.osGetDirs())
        self.checkHashFile() 
        
    def cleanupTestFolders(self):
        time.sleep(5)
        dirs = [directory, mntName]
        for dirname in dirs:
            dirpath = os.path.join(dirname)
            if os.path.exists(dirpath):
                shutil.rmtree(dirpath)
                print(f"cleanup {directory}")
                
    def checkAndCreateFolder(self, folder):
        path = os.path.join(workingPath, folder)
        isExist = os.path.exists(path)
        
        if not isExist:
            os.makedirs(path)
            print(f"creating {folder}")

    def copylogsToArtefactFolder(self):
            path = os.path.join(os.getcwd())
            files = glob.iglob(os.path.join(path,mntName,  self.currentDateForMnt(), "*.log"))
            for file in files:
                if os.path.isfile(file):
                    shutil.copy2(file, 'logs')

    def checkAndDeleteFolder(self, folder):
        dirpath = os.path.join(folder)
        
        if os.path.exists(dirpath):
            shutil.rmtree(dirpath)
            print(f"cleanup {directory}")

    def calculateFilesSize(self, start_path):
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(start_path):
            dir_size = 0
            for f in filenames:
                fp = os.path.join(dirpath, f)
                # skip if it is symbolic link
                if not os.path.islink(fp):
                    total_size += os.path.getsize(fp)
                    dir_size += os.path.getsize(fp)
            dir_size_mb = round(dir_size/1048576, 2)
            print(f"{dirpath} {dir_size_mb} MB")     
        print("Total ",round(total_size/1048576, 2), 'MB')
        return round(total_size/1048576, 2)