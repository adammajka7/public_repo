# walizkaIT

## spis treści

- [walizkaIT](#walizkait)
  - [spis treści](#spis-treści)
  - [o projekcie](#o-projekcie)
  - [instrukcja](#instrukcja)

## o projekcie

- Projekt ma na celu wczytanie konfiguracji i kopiwanie zawartych w pliku konfiguracyjnym repozytoriów do bezpiecznego miejsca. 
- Do projektu ma dostęp każdy pracownik z dostępem do Bitbucketa. 


## instrukcja

adres do repozytorium:
https://bitbucket.pzu.pl/projects/WAL/repos/repolist/browse

Aby dodać swoje repozytorium/repozytoria do pliku konfiguracyjnego kopii zapasowej należy odbić się od brancha master, nanieść swoje zmiany i stworzyć Pull Requesta do gałęzi master

podgląd przykładowego pliku z konfiguracją:

``` yaml
dane:
  repoData:
    - project: 
        name: "AD" # nazwa projektu
        repoRegex:   
          - '.+' # REGEX dla repozytorium
        branchRegex:
          - '^develop' # REGEX dla brancha
    - project: 
        name: "SSLS"
        repoRegex:   
          - '^sls-common'
        branchRegex:
          - '^master'
```

Załóżmy, że do pliku konfiguracyjnego chcemy dodać swój projekt (AAA) z repozytorium (AAArepo) i wybrać aby kopiowany był branch develop. 


Nasz projekt ma dres https://bitbucket.pzu.pl/projects/AAA
czyli nazwa projektu którą musimy użyć jest AAA

> nie zawsze nazwa projektu jest tożsama z tym co widzimy w adresie przeglądarki. Np
> projekt SLS ma w adresie SSLS (https://bitbucket.pzu.pl/projects/SSLS) czyli w tym przypadku w pliku konfiguracyjnym (repolist.yaml) musimy jako nazwę projektu wpisać SSLS, przykład : 
> ```yaml
>dane:
>  repoData:
>    - project: 
>        name: "SSLS"
>        repoRegex:   
>          - '.+'
>        branchRegex:
>          - '^develop'
>          - '^master'
>```


Odbijamy od brancha master np feature/projectAAA w pliku repolist.yaml dodajemy swój projekt:

```yaml
dane:
  repoData:
    - project: 
        name: "AD"
        repoRegex:   
          - '.+'
        branchRegex:
          - '^develop'
          - '^master'
    - project: 
        name: "SSLS"
        repoRegex:   
          - '^sls-common'
        branchRegex:
          - '^master'
    - project: # dodany nowy projekt
        name: "AAA" 
        repoRegex:   
          - '^AAArepo'
        branchRegex:
          - '^develop'
```

> jak można zauważyć `repoRegex` i `branchRegex` korzystają z wzorca [REGEX](https://pl.wikipedia.org/wiki/Wyrażenie_regularne) a swój wzorzec można przećwiczyć [tutaj](https://regexr.com)

następnie tworzymy Pull Requesta do gałęzi master po czym wystartuje proces weryfikujący danego PR. W jobie weryfikacyjnym PR możemy pobrać artefakt który zawiera log z procesu.

> aby móc odczytać artefakt lub log z procesu weryfikacyjnego należy posiadać odpowiednie uprawnienia w Bamboo. Takim uprawnieniem jest obecność w grupie AD Bamboo_WALIZKAIT_build_view do której zgłaszamy się poprzez CSZBI w projekcie Bamboo

![PR](images/pr1.JPG)
![PR](images/pr2.JPG)
![PR](images/pr3.JPG)


Po zmergowaniu przy następnym uruchomieniu procesu zostanie zastosowana nowa konfiguracja.
