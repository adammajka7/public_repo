package pl.pzu.jenkins.backup
import com.cloudbees.groovy.cps.NonCPS
import hudson.model.*
import groovy.json.JsonSlurper
import jenkins.model.Jenkins
import jenkins.plugins.http_request.HttpMode
import jenkins.security.CustomClassFilter
import org.jenkinsci.plugins.workflow.cps.CpsScript
import org.jvnet.hudson.plugins.thinbackup.restore.HudsonRestore
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import io.jenkins.plugins.casc.ConfigurationAsCode




class JenkinsBackup {
    public String backupDirPath = "/bitnami/jenkins/backup/"
    public String lazyMapCheckPath = "/external-generic-local/backup/"
    CpsScript w
    JenkinsBackup(CpsScript w) {
        this.w = w
    }

// thin backup działa tak, że najmłodszy folder musi być rozpakowany aby móc zrobić restore.
    def artifactoryBuild() {
        w.rtBuildInfo(
                // Optional - Maximum builds to keep in Artifactory.
                maxBuilds: 2,
                // Optional - Maximum days to keep the builds in Artifactory.
                maxDays: 2,
                // Optional - List of build numbers to keep in Artifactory.
                // Optional (the default is false) - Also delete the build artifacts when deleting a build.
                deleteBuildArtifacts: true,

                // Optional - Build name and build number. If not set, the Jenkins job's build name and build number are used.
                buildName: 'jenkinstest30',
                buildNumber: w.env.BUILD_NUMBER
                // Optional - Only if this build is associated with a project in Artifactory, set the project key as follows.
        )
    }

    def artifactoryPublish() {
        w.rtPublishBuildInfo(
                serverId: "artifactory"
        )
    }

    def backupFolderNameFromJenkinsUrl() {
        def url = w.env.JENKINS_URL
        def postfix = "/"
        String newUrl
        if (url.contains("http://")) {
            def prefix = "http://"
            newUrl = url - prefix - postfix
        } else if (url.contains("https://")) {
            def prefix = "https://"
            newUrl = url - prefix - postfix
        }
        return newUrl
    }

    def prepareArtifactoryFilesListNames(filesList) {
        def preparedList = []
        filesList.each {
            def preparedString = it - "backup/" - backupFolderNameFromJenkinsUrl() - "/"
            preparedList.add(preparedString)
        }
        return preparedList
    }

    def cleanUpAfterUpload() {
        def dir = new File(backupDirPath)
        dir.list().each {
            w.println("usuwam lokalnie $it")
            new File(backupDirPath + it).delete()
        }
    }

    def artifactoryCheckFilesAndUpload( List compare_list) {
        def dir = new File(backupDirPath)

        dir.list().each {
            def url = backupFolderNameFromJenkinsUrl()

            String localFile = it
            String fullLocalFile = "backup/"+url+"/"+it
            if (it.contains(".zip")) {
                if (!compare_list.contains(fullLocalFile)) {
                    w.println("nie ma $fullLocalFile pipeline artifactory $compare_list, wysylam")
                    w.rtUpload(
                            serverId: "artifactory",
                            spec: '''{
                        "files": [
                            {
                                "pattern": "''' + backupDirPath + localFile + '''",
                                "target": "external-generic-local/backup/''' + backupFolderNameFromJenkinsUrl() + '''/"
                            }
                        ]
                    }'''
                    )
                } else {
                    w.println("$localFile wystepuje pipeline arti, pomijam")
                }
            }
        }
    }

    def backupFilesListFromArtifactory(auth, url) {
        def response = w.httpRequest authentication: auth, url: url
        def jsonSlurper = new JsonSlurper()
        def jsonInfo = jsonSlurper.parseText(response.content)
        def fileList = jsonInfo.files
        return fileList
    }

    def deleteFilesFromArtifactory(auth, url) {
        def response = w.httpRequest authentication: auth, url: url
        w.httpRequest authentication: auth, url: url, httpMode: HttpMode.DELETE
    }

    def changeNameBackupsetToFull() {
        def path = backupDirPath
        def dh = new File(path)
        dh.list().each {
            if (it.contains("BACKUPSET")) {
                def newFile = new File("$path/$it")
                def changeName = it.replace("BACKUPSET_", "FULL-").replace("_.zip", "_UTC.zip")
                w.println("zmieniam nazwe z $newFile na $changeName")
                newFile.renameTo("$path/$changeName")
            }
        }
    }

    def zipFolder() {
        w.dir(backupDirPath) {
            def files =  w.findFiles()
            def directory = files.find { it.directory }
            if (directory == null) {
                w.println("nie ma folderu, pomijam")
            } else {
                def path = backupDirPath
                File checkFileExist = new File(backupDirPath+directory.name + "_UTC.zip")
                if (checkFileExist.exists()){
                    checkFileExist.delete()
                }else{
                    w.zip(dir: directory.path, zipFile: directory.name + "_UTC.zip")
                    w.println("kasuje $backupDirPath$directory.name")
                    def deletePath = new File("$backupDirPath${directory.name}")
                    deletePath.deleteDir()

                }


            }
        }
    }

    def cleanUpArtifactory(ArrayList artifactoryListBackup) {
        int number = 0
        artifactoryListBackup.each { backupItem ->
            number++
        }
    }
    def prepareAddressBackupPrefixForRestore(String stringItem){
        String backupFolderName = backupFolderNameFromJenkinsUrl()
        def preparedString = stringItem - "backup/${backupFolderName}"
        def preparedStringWithoutZip = stringItem - "backup/${backupFolderName}" - ".zip"
        def preparedStringWithoutZipAndUtc = stringItem - "backup/${backupFolderName}" - ".zip" - "_UTC"
        return [preparedString, preparedStringWithoutZip, preparedStringWithoutZipAndUtc]
    }
    def removeOldestArtifactFromArtifactory(){
        ArrayList preparedList = []
        String backupFolderName = backupFolderNameFromJenkinsUrl()
        def dateNowMinus = new Date().minus(7)
        def dateFormat = "yyyy-MM-dd_HH-mm"

        ArrayList fileNames = backupFilesListFromArtifactory('artifactory-credencial',
                "https://artifactory.pzu.pl/artifactory/api/search/pattern?pattern=external-generic-local:backup/${backupFolderNameFromJenkinsUrl()}/*")
        fileNames.each { address ->
            String preparedAddress = address - "backup/${backupFolderName}/FULL-" - "_UTC.zip"
            def prepareDateString =  new SimpleDateFormat(dateFormat).parse(preparedAddress)
            preparedList.add(prepareDateString)
            if(prepareDateString < dateNowMinus){
                w.println("USUWAM !! ${prepareDateString.format(dateFormat)} bo jest mniejsze od ${dateNowMinus.format(dateFormat)}")
                deleteFilesFromArtifactory('artifactory-credencial',
                        "https://artifactory.pzu.pl/artifactory/external-generic-local/backup/${backupFolderName}/FULL-${prepareDateString.format(dateFormat)}_UTC.zip")
            }
        }
    }

    def sortList(ArrayList list){
        return list.sort()
    }

    def restoreBackup(String restoreItem){
        def restorePath = "/bitnami/jenkins/backup/"
        println("restoring $restoreItem")
        w.httpRequest url: 'https://artifactory.pzu.pl/artifactory/external-generic-local/'+restoreItem,
                outputFile: restorePath+prepareAddressBackupPrefixForRestore(restoreItem)[0],
                httpMode: HttpMode.GET
        w.unzip zipFile: restorePath+prepareAddressBackupPrefixForRestore(restoreItem)[0], dir: restorePath+prepareAddressBackupPrefixForRestore(restoreItem)[2]
        String backupPath = restorePath+prepareAddressBackupPrefixForRestore(restoreItem)[1]

    }
    def reloadJcac(){
        ConfigurationAsCode.get().configure()
    }

    void doCancelQuitJob(){
        Jenkins.getInstanceOrNull().doCancelQuietDown()
    }

}

