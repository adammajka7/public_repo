package pl.pzu.jenkins.tools

import com.fasterxml.jackson.databind.ObjectMapper
import hudson.model.ParameterDefinition
import org.apache.commons.lang.RandomStringUtils
import org.jenkinsci.plugins.workflow.cps.CpsScript
import hudson.EnvVars;
import hudson.slaves.EnvironmentVariablesNodeProperty;
import hudson.slaves.NodeProperty;
import hudson.slaves.NodePropertyDescriptor;
import hudson.util.DescribableList;
import jenkins.model.Jenkins;


class JenkinsTools {
    CpsScript pipeline
    JenkinsTools(CpsScript w) {
        this.pipeline = w
    }

    void printParams(Object o) {
        o.each {
            pipeline.println(it)
        }
    }
    void createGlobalEnvironmentVariables(String key, String value){

        Jenkins instance = Jenkins.getInstanceOrNull()

        DescribableList<NodeProperty<?>, NodePropertyDescriptor> globalNodeProperties = instance.getGlobalNodeProperties()
        List<EnvironmentVariablesNodeProperty> envVarsNodePropertyList = globalNodeProperties.getAll(EnvironmentVariablesNodeProperty.class)

        EnvironmentVariablesNodeProperty newEnvVarsNodeProperty = null
        EnvVars envVars = null

        if ( envVarsNodePropertyList == null || envVarsNodePropertyList.size() == 0 ) {
            newEnvVarsNodeProperty = new hudson.slaves.EnvironmentVariablesNodeProperty()
            globalNodeProperties.add(newEnvVarsNodeProperty)
            envVars = newEnvVarsNodeProperty.getEnvVars()
        } else {
            envVars = envVarsNodePropertyList.get(0).getEnvVars()
        }
        envVars.put(key, value)
        instance.save()
    }
    List<String> namespacesList(){
        List<String> listNamespaces = ["app-mako",
                      "app-makoschool-test",
                      "app-mako-test",
                      "app-mako-tools"]
        return listNamespaces
    }

    String putSahulUser(String namespace){
        if(namespace == "app-mako" || namespace == "app-mako-tools"){
            String userID = "marlin-sahul-prod"
            return userID
        }else{
            String userID = "marlin-sahul-nonprod"
            return userID
        }
    }

    List<String> deploymentsList(String namespace,
                         String credentialsId){
        pipeline.withCredentials([
                pipeline.usernamePassword(
                        credentialsId: "${credentialsId}",
                        usernameVariable: "K8S_USERNAME",
                        passwordVariable: "K8S_PASSWORD"
                )
        ]) {
            def filename = fileNameGenerator()
            List<String> deploymentsList = []
            Map<String, String> mapka = [:]
            pipeline.sh("export K8S_CLUSTER=sahul-cicd; " +
                    "cd ${pipeline.env.WORKSPACE}; " +
                    "curl -sfLO https://harbor.pzu.pl/sahul/login.sh && source ./login.sh --k8slogin; " +
                    "kubectl get deployments -n ${namespace} -o yaml > ${filename}; " +
                    "unset K8S_CLUSTER; unset K8S_USERNAME; unset K8S_PASSWORD")
            def data = pipeline.readYaml file: "${filename}"
            pipeline.sh("rm -f ${filename}")
            data['items']['metadata']['name'].each {
                deploymentsList.add(it)
            }

            return deploymentsList
        }
    }

    Map<String, String> ingressList(String deployNameKube,String namespace,
                                 String credentialsId){
        pipeline.withCredentials([
                pipeline.usernamePassword(
                        credentialsId: "${credentialsId}",
                        usernameVariable: "K8S_USERNAME",
                        passwordVariable: "K8S_PASSWORD"
                )
        ]) {
            def filename = fileNameGenerator()
            List<String> deploymentsList = []
            Map<String, String> mapka = [:]
            pipeline.sh("export K8S_CLUSTER=sahul-cicd; " +
                    "cd ${pipeline.env.WORKSPACE}; " +
                    "curl -sfLO https://harbor.pzu.pl/sahul/login.sh && source ./login.sh --k8slogin; " +
                    "kubectl get ingress -n ${namespace} -o yaml > ${filename}; " +
                    "unset K8S_CLUSTER; unset K8S_USERNAME; unset K8S_PASSWORD")
            def data = pipeline.readYaml file: "${filename}"
            pipeline.sh("rm -f ${filename}")
            data['items']['spec'].each {
                String ingress = it['rules']['host'][0]
                String deployName = it['rules']['http']['paths']['backend']['serviceName'][0][0]
                mapka["${deployName}"] = ingress
            }
            return mapka
        }
    }

    String fileNameGenerator(){
        String charset = (('A'..'Z') + ('0'..'9')).join()
        Integer length = 10
        String randomString = RandomStringUtils.random(length, charset.toCharArray())
        String filename = "${randomString}.yaml"
        return filename
    }

    List showFreeAddresses(List<String> namespaces){
        String credentialsId

        List<String> listUsedIngresses = []
        List<String> listAllAddresses = []
        List<String> freeAddresses = []
        for (int i = 1; i <= 30; i++) {
            if(i<10){
                listAllAddresses.add("mako0${i}.sahul.pzu.pl")
            }else{
                listAllAddresses.add("mako${i}.sahul.pzu.pl")
            }

        }
        namespaces.each { namespace ->
            credentialsId = putSahulUser(namespace)
            pipeline.withCredentials([
                    pipeline.usernamePassword(
                            credentialsId: "${credentialsId}",
                            usernameVariable: "K8S_USERNAME",
                            passwordVariable: "K8S_PASSWORD"
                                            )
                                    ]) {
                def filename = fileNameGenerator()

                pipeline.sh("export K8S_CLUSTER=sahul-cicd; " +
                        "cd ${pipeline.env.WORKSPACE}; " +
                        "curl -sfLO https://harbor.pzu.pl/sahul/login.sh && source ./login.sh --k8slogin; " +
                        "kubectl get ingress -n ${namespace} -o yaml > ${filename}; " +
                        "unset K8S_CLUSTER; unset K8S_USERNAME; unset K8S_PASSWORD")
                def data = pipeline.readYaml file: "${filename}"
                pipeline.sh("rm -f ${filename}")
                data['items']['spec'].each {
                    String ingress = it['rules']['host'][0]
                    listUsedIngresses.add(ingress)
                }
            }
        }
        freeAddresses = listAllAddresses - listUsedIngresses
        return freeAddresses
    }

    String feeAddressesForInfo(List freeAddresses){
        return "\nfree addresses: ${freeAddresses}\n"
    }


    String getIngressFromDeployName(String deployName, String namespace, String credentialsId){
        Map<String, String> mapka = ingressList(deployName, namespace, credentialsId)
        String ingressFromDeployname = mapka.get(deployName)
        return ingressFromDeployname
    }

    void createJenkinsInstance(String script,
                               String jenkinsName,
                               String ingressAdress,
                               String namespace,
                               String credentialsId,
                               String jcacText,
                               String secretsText){

        pipeline.withCredentials([
                pipeline.usernamePassword(
                        credentialsId: "${credentialsId}",
                        usernameVariable: "K8S_USERNAME",
                        passwordVariable: "K8S_PASSWORD"
                )
        ]) {
            String fullPath = pipeline.env.WORKSPACE + "/"+script
            pipeline.writeFile(file: "${pipeline.env.WORKSPACE}/casc_config/jcac/${jenkinsName}.yaml", text: jcacText)
            pipeline.writeFile(file: "${pipeline.env.WORKSPACE}/casc_config/properties/${jenkinsName}.properties", text: secretsText)
            pipeline.sh("export K8S_CLUSTER=sahul-cicd; " +
                    "cd ${pipeline.env.WORKSPACE}; " +
                    "curl -sfLO https://harbor.pzu.pl/sahul/login.sh && source ./login.sh --k8slogin; " +
                    "${fullPath} -i ${jenkinsName} -a ${ingressAdress} -n ${namespace} -s; " +
                    "unset K8S_CLUSTER; unset K8S_USERNAME; unset K8S_PASSWORD")
        }
    }
    void deleteJenkinsInstance(String script,
                               String jenkinsName,
                               String namespace,
                               String credentialsId){

        pipeline.withCredentials([
                pipeline.usernamePassword(
                        credentialsId: "${credentialsId}",
                        usernameVariable: "K8S_USERNAME",
                        passwordVariable: "K8S_PASSWORD"
                )
        ]) {
            String fullPath = pipeline.env.WORKSPACE + "/"+script
            pipeline.sh("export K8S_CLUSTER=sahul-cicd; " +
                    "cd ${pipeline.env.WORKSPACE}; " +
                    "curl -sfLO https://harbor.pzu.pl/sahul/login.sh && source ./login.sh --k8slogin; " +
                    "${fullPath} -i ${jenkinsName} -n ${namespace} -x; " +
                    "unset K8S_CLUSTER; unset K8S_USERNAME; unset K8S_PASSWORD")
        }
    }

    void updateJenkinsInstance(String script,
                               String jenkinsName,
                               String namespace,
                               String credentialsId){

        pipeline.withCredentials([
                pipeline.usernamePassword(
                        credentialsId: "${credentialsId}",
                        usernameVariable: "K8S_USERNAME",
                        passwordVariable: "K8S_PASSWORD"
                )
        ]) {
            String fullPath = pipeline.env.WORKSPACE + "/"+script
            pipeline.sh("export K8S_CLUSTER=sahul-cicd; " +
                    "cd ${pipeline.env.WORKSPACE}; " +
                    "curl -sfLO https://harbor.pzu.pl/sahul/login.sh && source ./login.sh --k8slogin; " +
                    "${fullPath} -i ${jenkinsName} -n ${namespace} -x; " +
                    "unset K8S_CLUSTER; unset K8S_USERNAME; unset K8S_PASSWORD")
        }
    }
    def configFilesRead(String jcacFileName, String secretsFileName){
        List configFilesArray = []
        def jcacFileText = pipeline.readFile(file: "${pipeline.env.WORKSPACE}/casc_config/jcac/${jcacFileName}")
        def secretsFileText = pipeline.readFile(file: "${pipeline.env.WORKSPACE}/casc_config/properties/${secretsFileName}")
        configFilesArray.add(jcacFileText)
        configFilesArray.add(secretsFileText)
        return configFilesArray
    }

    def jcacFileRead(String jenkinsName){
        def dataFile = pipeline.readFile(file: "${pipeline.env.WORKSPACE}/casc_config/jcac/${jenkinsName}_downloaded.yaml")
        return dataFile
    }

    def secretsFileRead(String jenkinsName){
        def dataFile = pipeline.readFile(file: "${pipeline.env.WORKSPACE}/casc_config/properties/${jenkinsName}_downloaded.properties")
        return dataFile
    }

    def ovFileRead(){
        def dataFile = pipeline.readFile(file: "${pipeline.env.WORKSPACE}/jenkinstools/ov.yaml")
        return dataFile
    }

    void ovFileWrite(String fileText){
        def dataFile = pipeline.writeFile(file: "${pipeline.env.WORKSPACE}/jenkinstools/ov.yaml", text: fileText)
    }

    void jcacFileWrite(String jenkinsName, String fileText){
        def dataFile = pipeline.writeFile(file: "${pipeline.env.WORKSPACE}/casc_config/jcac/${jenkinsName}_downloaded.yaml", text: fileText)
    }

    void secretsFileWrite(String jenkinsName, String fileText){
        def dataFile = pipeline.writeFile(file: "${pipeline.env.WORKSPACE}/casc_config/properties/${jenkinsName}_downloaded.properties", text: fileText)
    }

    void downloadUploadAllConfig(String downloadOrUpload,
                                 String script,
                                 String jenkinsName,
                                 String namespace,
                                 String credentialsId){
        pipeline.withCredentials([
                pipeline.usernamePassword(
                        credentialsId: "${credentialsId}",
                        usernameVariable: "K8S_USERNAME",
                        passwordVariable: "K8S_PASSWORD"
                )
        ]) {
            String fullPath = pipeline.env.WORKSPACE + "/"+script
            String statusDU
            if(downloadOrUpload=="download"){
                statusDU= "download"
            }else {
                statusDU = "upload"
            }
            pipeline.sh("export K8S_CLUSTER=sahul-cicd; " +
                    "cd ${pipeline.env.WORKSPACE}; " +
                    "curl -sfLO https://harbor.pzu.pl/sahul/login.sh && source ./login.sh --k8slogin; " +
                    "${fullPath} -i ${jenkinsName} -n ${namespace} -j ${statusDU}_all_x; " +
                    "unset K8S_CLUSTER; unset K8S_USERNAME; unset K8S_PASSWORD")
        }
    }

    String namespaceInfo(String namespace,
                         String credentialsId){

        pipeline.withCredentials([
                pipeline.usernamePassword(
                        credentialsId: "${credentialsId}",
                        usernameVariable: "K8S_USERNAME",
                        passwordVariable: "K8S_PASSWORD"
                )
        ]) {
            def fileName = fileNameGenerator()
            pipeline.sh("export K8S_CLUSTER=sahul-cicd; " +
                    "cd ${pipeline.env.WORKSPACE}; " +
                    "curl -sfLO https://harbor.pzu.pl/sahul/login.sh && source ./login.sh --k8slogin; " +
                    "kubectl describe namespace ${namespace} > ${fileName}; " +
                    "unset K8S_CLUSTER; unset K8S_USERNAME; unset K8S_PASSWORD")
            String data = pipeline.readFile(file: "${pipeline.env.WORKSPACE}/${fileName}")
            List dataList = data.split("Resource Quotas")
            String resourcesText = "${namespace} resources:\n" + dataList[1] - "No LimitRange resource."
            return resourcesText
        }
    }

    void runK8sCommand(String command, String credentialsId){
        pipeline.withCredentials([
                pipeline.usernamePassword(
                        credentialsId: "${credentialsId}",
                        usernameVariable: "K8S_USERNAME",
                        passwordVariable: "K8S_PASSWORD"
                )
        ]) {
            pipeline.sh("export K8S_CLUSTER=sahul-cicd; " +
                    "cd ${pipeline.env.WORKSPACE}; " +
                    "curl -sfLO https://harbor.pzu.pl/sahul/login.sh && source ./login.sh --k8slogin; " +
                    "${command}; " +
                    "unset K8S_CLUSTER; unset K8S_USERNAME; unset K8S_PASSWORD")
        }
    }

    List<ParameterDefinition> deploymentListParameters(List<String> deployments){
        List<ParameterDefinition> paramDeployments = new ArrayList<>()
        deployments.each {paramDeployments.add(pipeline.booleanParam(it))}
        return paramDeployments
    }

    String httpRequestGet(auth, url, apiUrl) {
        String postfixUrl = apiUrl
        String prefixUrl = "https://${url}"
        String finalUrl = prefixUrl+postfixUrl
        def response = pipeline.httpRequest authentication: auth, url: finalUrl
        String responseInfo = response.content
        return responseInfo
    }

    String httpRequestPost(auth, url, apiUrl) {
        String postfixUrl = apiUrl
        String prefixUrl = "https://${url}"
        String finalUrl = prefixUrl+postfixUrl
        def response = pipeline.httpRequest authentication: auth, url: finalUrl, httpMode: "POST"
        String responseInfo = response.content
        return responseInfo
    }

    Boolean activeJobsExist(String jsonText) {
        boolean jobExist = false
        boolean isNull = false
        ObjectMapper objectMapper = new ObjectMapper()
        Map<String, Object> parametersMap = objectMapper.readValue(jsonText, Map.class)

        parametersMap.jobs.each {
            if(it.get('color') != null){
                if(it.get('color').contains('anime')){
                    jobExist = true
                }
            }
        }
        return jobExist
    }

    void waitUntilisNotBussy(auth, url, apiUrl){
        boolean wait = true
        ObjectMapper objectMapper = new ObjectMapper()
        while (wait==true){
            String jsonText = httpRequestGet(auth, url, apiUrl)
            if(activeJobsExist(jsonText)==true){
                wait = true
            }else{
                wait = false
                httpRequestPost(auth, url, "/quietDown")
            }

            Map<String, Object> parametersMap = objectMapper.readValue(jsonText, Map.class)
            parametersMap.jobs.each {
                if(it.get('color') != null){
                    if(it.get('color').contains('anime')){
                        pipeline.println("waiting for ${it.get('url')}")
                    }
                }
            }
            sleep(5000)
        }
    }

}
