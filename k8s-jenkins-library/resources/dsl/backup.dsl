def bitbucket_url = "https://bitbucket.pzu.pl/scm/mako/k8s-jenkins-library.git"
def branchPut = "develop"

def folderName = "Backup"
folder(folderName)
pipelineJob("Backup/backup") {
   description()
   keepDependencies(false)
   definition {
      cpsScm {
          triggers {
              cron "0 3 * * *"
          }
         scm {
            git {
               remote {
                  url(bitbucket_url)
                  credentials("k8s-jenkins-library")
               }
               branch(branchPut)
            }
         }
         scriptPath("resources/pipelines/callbackup.pipeline")
      }
   }
   disabled(false)
}
pipelineJob("Backup/restore") {
   description()
   keepDependencies(false)
   definition {
      cpsScm {
         scm {
            git {
               remote {
                  url(bitbucket_url)
                  credentials("k8s-jenkins-library")
               }
               branch(branchPut)
            }
         }
         scriptPath("resources/pipelines/callrestore.pipeline")
      }
   }
   disabled(false)
}

