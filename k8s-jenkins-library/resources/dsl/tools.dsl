def bitbucket_url = "https://bitbucket.pzu.pl/scm/mako/k8s-jenkins-library.git"
def branchPut = WORK_BRANCH

def folderName = "Tools"
folder(folderName)
pipelineJob("Tools/create") {
   description()
   keepDependencies(false)
   definition {
      cpsScm {
         scm {
            git {
               remote {
                  url(bitbucket_url)
                  credentials("k8s-jenkins-library")
               }
               branch(branchPut)
            }
         }
         scriptPath("resources/pipelines/calltoolscreate.pipeline")
      }
   }
   disabled(false)
}
pipelineJob("Tools/delete") {
   description()
   keepDependencies(false)
   definition {
      cpsScm {
         scm {
            git {
               remote {
                  url(bitbucket_url)
                  credentials("k8s-jenkins-library")
               }
               branch(branchPut)
            }
         }
         scriptPath("resources/pipelines/calltoolsdelete.pipeline")
      }
   }
   disabled(false)
}
pipelineJob("Tools/update") {
   description()
   keepDependencies(false)
   definition {
      cpsScm {
         scm {
            git {
               remote {
                  url(bitbucket_url)
                  credentials("k8s-jenkins-library")
               }
               branch(branchPut)
            }
         }
         scriptPath("resources/pipelines/calltoolsupdate.pipeline")
      }
   }
   disabled(false)
}
pipelineJob("Tools/reload") {
   description()
   keepDependencies(false)
   definition {
      cpsScm {
         scm {
            git {
               remote {
                  url(bitbucket_url)
                  credentials("k8s-jenkins-library")
               }
               branch(branchPut)
            }
         }
         scriptPath("resources/pipelines/calltoolsmulti.pipeline")
      }
   }
   disabled(false)
}


