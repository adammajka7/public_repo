#!/usr/bin/env python
import time, glob, shutil, yaml, subprocess, requests, os
from tkinter import *
from tkinter import filedialog, ttk
from pytube import YouTube, Playlist
from pathlib import Path
from datetime import datetime, timedelta
from moviepy.video.io.VideoFileClip import VideoFileClip, AudioFileClip
from moviepy.video.VideoClip import ImageClip
from moviepy.video.compositing.CompositeVideoClip import CompositeVideoClip
from threading import *
from shutil import copyfile

__version__ = '1.0.9'
wiersz = 0
root = Tk()
with open('config.yml', 'r') as fp:
    configVersion = yaml.safe_load(fp)

# add version to config
def set_state(state):
    file_name = "config.yml"
    with open(file_name) as f:
        doc = yaml.safe_load(f)
    doc['youtubeDownloader']['version'] = state
    with open(file_name, 'w') as f:
        yaml.safe_dump(doc, f, default_flow_style=False)
set_state(__version__)

root.title(f"Jacek Sciągacz")
root.geometry('800x630')

pb = ttk.Progressbar(
    root,
    orient='horizontal',
    mode='determinate',
    length=280, 
)

l = Label(root, text='wklej linki')
l.grid(row=0, columnspan=4)
l.config(anchor=CENTER)
lversion = Label(root, text=f"v{__version__}")
lversion.grid(row=4, column=3, sticky=E, padx=10)
t = Text(root, width=100)
t.grid(row=1, columnspan=4)
console = Text(root, width=100, height=10)
console.grid(row=3, columnspan=4)

class youtubeDownloaderC:
    
    def cleanup(self):
        self.tmpFiles = glob.glob(self.dirTMP+'/*mp3')
        self.SRC_DIR = self.dirTMP
        self.TARG_DIR = self.dir
        self.GLOB_PARMS = "*.mp3"

        for file in glob.glob(os.path.join(self.SRC_DIR, self.GLOB_PARMS)):
            if file not in glob.glob(os.path.join(self.TARG_DIR, self.GLOB_PARMS)):
                shutil.copy(file, self.TARG_DIR)
            else:
                print("{} exists in {}".format(
                    file, os.path.join(os.path.split(self.TARG_DIR)[-2:])))

        try:
            shutil.rmtree(self.dirTMP)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))
            
    def konfig(self):
        with open('config.yml', 'r') as fp:
            self.config = yaml.safe_load(fp)
        if self.config['youtubeDownloader']['copyToDesktop']:
            self.configStatus = True
        else:
            self.configStatus = False
        if self.configStatus:
            print(f'miejsce docelowe - config')
            self.showLog(f'miejsce docelowe - config')
        else:
            print(f'miejsce docelowe - folder download')
            self.showLog(f'miejsce docelowe - folder download')
        return self.configStatus

    def ytlinks(self):
        self.f = open('youtubelinks.txt', 'r')
        self.lines = self.f.readlines()
        return self.lines

    def prepareFolders(self):
        if self.konfig():
            self.dir = self.config['youtubeDownloader']['copyPath']
        else:
            self.dir = 'downloads'
            Path(self.dir).mkdir(parents=True, exist_ok=True)

    def prepareTMP(self):
        self.dirTMP = 'TMP'
        self.dirFiles = glob.glob(self.dir+'/*mp3')
        Path(self.dirTMP).mkdir(parents=True, exist_ok=True)
    
    def percent(self, tem, total):
        perc = (float(tem) / float(total)) * float(100)
        return perc
    
    def progress_function(stream, chunk, file_handle, bytes_remaining):
        pb.grid(columnspan=4, row=4, padx=10, pady=5)
        print(round((1-bytes_remaining/stream.video.filesize)*100), '% done...')
        progress = round((1-bytes_remaining/stream.video.filesize)*100)
        pb['value'] = progress
        if progress == 100:
            pb.stop()
            pb.grid_forget()
            
    def startDownload(self):
        for l in self.lines:
            if 'list=' in l:
                playlist = Playlist(l)
                playlistItems = len(playlist)
                i = 1
                self.showLog(f'!! wykryłem link Playlist - ilość utworów {playlistItems}')
                for song in playlist:
                    
                    try:
                        self.yt = YouTube(song, on_progress_callback=self.progress_function)    
                    except:
                        youtubeDownloaderC.showLog('self', '!! blad w srodku 1 try - song')
                        
                    while True:
                        try:
                            self.video = self.yt.streams.first()
                            break
                        except:
                            youtubeDownloaderC.showLog('self', '!! blad YT stream - ponawiam próbę...')
                            
                    if not self.dirFiles:
                        print('preparte table')
                    self.dirFiles.append('empty')
                    self.videoTitle = self.video.title.replace('/', '').replace('.', '')
                    
                    if any(self.videoTitle in s for s in self.dirFiles):
                        print(f'!! POMIJAM istnieje plik {i}/{playlistItems} - {self.videoTitle}')
                        self.showLog(f'!! POMIJAM istnieje plik {i}/{playlistItems} - {self.videoTitle}')
                    else:
                        
                        print(f"## SCIAGAM plik {i}/{playlistItems}- {self.videoTitle}")
                        self.showLog(f"## SCIAGAM plik {i}/{playlistItems} - {self.videoTitle}")
                        # check for destination to save file
                        self.destination = self.dirTMP
                        # download the file
                        self.out_file = self.video.download(
                            output_path=self.destination)
                        self.mp4_file = self.out_file
                        self.base, self.ext = os.path.splitext(self.out_file)
                        self.new_file = self.base + '.mp3'
                        self.mp3_file = self.new_file
                        
                        with open('config.yml', 'r') as fp:
                            self.config = yaml.safe_load(fp)
                            if self.config['youtubeDownloader']['convert'] == False:
                                print('nie konwertuje')
                            else:
                                self.videoclip = AudioFileClip(self.mp4_file)
                                self.audioclip = self.videoclip
                                print(f">> KONWERTUJE DO MP3")
                                self.showLog(f">> KONWERTUJE DO MP3")
                                
                                self.audioclip.write_audiofile(self.mp3_file)
                                
                                self.audioclip.close()
                                self.videoclip.close()
                                print(f">> KONIEC PRACY Z {self.videoTitle}")
                                self.showLog(f"-- SKOŃCZYŁEM Z {self.videoTitle}")
                                self.cleanup()
                    i += 1
            else:
                self.yt = YouTube(l, on_progress_callback=self.progress_function)
                # extract only audio
                self.video = self.yt.streams.filter(only_audio=True).first()
                
                if not self.dirFiles:
                    print('preparte table')
                    self.dirFiles.append('empty')
                    
                self.videoTitle = self.video.title.replace('/', '').replace('.', '')
                
                if any(self.videoTitle in s for s in self.dirFiles):
                    print(f'!! POMIJAM istnieje plik - {self.videoTitle}')
                    self.showLog(f'!! POMIJAM istnieje plik - {self.videoTitle}')
                else:
                    print(f"## SCIAGAM plik - {self.videoTitle}")
                    self.showLog(f"## SCIAGAM plik - {self.videoTitle}")
                    self.destination = self.dirTMP
                    # download the file
                    self.out_file = self.video.download(output_path=self.destination)
                    self.mp4_file = self.out_file
                    self.base, self.ext = os.path.splitext(self.out_file)
                    self.new_file = self.base + '.mp3'
                    self.mp3_file = self.new_file
                    print('self.mp3_file: ', self.mp3_file)
                    
                    with open('config.yml', 'r') as fp:
                                self.config = yaml.safe_load(fp)
                                if self.config['youtubeDownloader']['convert'] == False:
                                    print('nie konwertuje')
                                else:
                                    self.videoclip = AudioFileClip(self.mp4_file)
                                    self.audioclip = self.videoclip
                                    print(f">> KONWERTUJE DO MP3")
                                    self.showLog(f">> KONWERTUJE DO MP3")
                                    self.audioclip.write_audiofile(self.mp3_file)
                                    self.audioclip.close()
                                    self.videoclip.close()
                                    print(f">> KONIEC PRACY Z {self.videoTitle}")
                                    self.showLog(f">> KONIEC PRACY Z {self.videoTitle}")
                                    self.cleanup()
                    
    def showLog(self, text):
        global wiersz
        wiersz += 1
        console.insert(END, str(wiersz)+': '+text+'\n')
        root.update()
        console.see(END)

    def stopProgram(self):
        print(f"KONIEC PRACY PROGRAMU")
        self.showLog(f"KONIEC PRACY")
        l = 3

class Updater:
    versionTable = []
    def updateCheck(self):
        url = 'http://serverip:8081/'
        ext = 'zip'

        def listFD(url, ext):
            page = requests.get(url).text
            # print('page przed', page)
            pager = page.replace('<a href="', '').replace('<hed><title>Index of /</title></hed>', '').replace('<body>', '').replace('<html>','').replace('<head><title>Index of /</title></head>', '').replace('<h1>Index of /</h1><hr><pre>../">../</a>','').replace(' ','').replace('</pre><hr></body>', '').replace('</html>','')
            # print('page po', pager)
            pagesplit = pager.split('\n')
            # print('pagesplit', pagesplit)
            for pag in pagesplit:
                if pag == '\r':
                    pass
                else:
                    if pag == '':
                        pass
                    else:

                        self.versionTable.append(pag[0:5])
            return self.versionTable 
            
        self.vt = listFD(url, ext)
        print('vt', self.vt)
        
        def searchTopVersion(self, listWithVersion):
            for element in listWithVersion:
                v1 = element[0:1]
                v2 = element[2:3]
                v3 = element[4:5]
                v1Top = v1 
                v2Top = v2
                v3Top = v3
                if v1 <= v1Top:
                    pass
                else:
                    v1Top = v1
                if v2 <= v2Top:
                    pass
                else:
                    v2Top = v2
                if v3 <= v3Top:
                    pass
                else:
                    v3Top = v3
                
                print('element', v1, v2, v3)
                print('elementTop', v1Top, v2Top, v3Top)
            return(f'{v1Top}.{v2Top}.{v3Top}')
        
        
        
        TopVersion = searchTopVersion(self, self.vt)
        file_name = "config.yml"
        with open(file_name) as f:
            doc = yaml.safe_load(f)
        doc['youtubeDownloader']['topVersion'] = TopVersion
        
        
        with open(file_name, 'w') as f:
            yaml.safe_dump(doc, f, default_flow_style=False)
        print('TopVersion is:', TopVersion)
        print('__version__ is:', __version__)
        if TopVersion <= __version__:
            print('a')
        else:
            print('b')
            global updaterWindow
            updaterWindow = Tk()
            updaterWindow.lift()
            updaterWindow.attributes('-topmost',True)
            updaterWindow.title(f"Jacek Sciągacz - updater")
            updaterWindow.geometry('400x200')
            l = Label(updaterWindow, text='jest aktualizacja do sciagniecia')
            l.grid(row=0, column=0, )
            l1 = Label(updaterWindow, text=f'twoja wersja to {__version__}, najnowsza wersja to {TopVersion}')
            l1.grid(row=1, column=0, )
            b = Button(updaterWindow, text='aktualizuj', command=self.button_click)
            b.grid(row=2, column=0)
    def button_click(self):
        print('button1')
        root.destroy()
        updaterWindow.destroy()
        os.startfile('updater.exe')       

def run_updater():
    up = Updater()
    up.updateCheck()

run_updater()

def main():
    
    YT = youtubeDownloaderC()
    YT.ytlinks()
    YT.prepareFolders()
    YT.prepareTMP()
    YT.startDownload()
    YT.stopProgram()
    
    # YT.cleanup()
    # try:
    #     YT = youtubeDownloaderC()
    # except:
    #     print("blad w srodku 2 try")
    #     youtubeDownloaderC.showLog(
    #         'self', '!! blad 1')
    # try:
    #     YT.ytlinks()
    # except:
    #     print("blad w srodku 2 try")
    #     youtubeDownloaderC.showLog(
    #         'self', '!! blad 2')
    # try:
    #     YT.prepareFolders()
    # except:
    #     print("blad w srodku 2 try")
    #     youtubeDownloaderC.showLog(
    #         'self', '!! blad 3')
    # try:
    #     YT.prepareTMP()
    # except:
    #     print("blad w srodku 2 try")
    #     youtubeDownloaderC.showLog(
    #         'self', '!! blad 4')
    # try:
    #     YT.startDownload()
    # except:
    #     print("blad w srodku 2 try")
    #     youtubeDownloaderC.showLog(
    #         'self', '!! blad 5')
    # try:
    #     YT.stopProgram()
    # except:
    #     print("blad w srodku 2 try")
    #     youtubeDownloaderC.showLog(
    #         'self', '!! blad 6')


def threading():
    # Call work function
    t1 = Thread(target=download_function)
    t1.start()


def download_function():
    b.config(state=DISABLED)
    f = open('youtubelinks.txt', 'w')
    read = t.get("1.0", "end").split('\n')
    for r in read:
        if not r:
            continue
        else:
            print('wiersz ', r)
            f.write(r+'\n')
    f.close()
    main()
    b.config(state=ACTIVE)


def bpaste_function():
    text = root.clipboard_get()
    t.insert('end', text+'\n')


def clear_function():
    b.config(state=ACTIVE)
    t.delete("1.0", "end")
    console.delete("1.0", "end")
def reset_function():
    b.config(state=ACTIVE)
    console.delete("1.0", "end")

# settings
def pathFromConfig():
    with open('config.yml', 'r') as fp:
        config = yaml.safe_load(fp)
        dir = config['youtubeDownloader']['copyPath']
        return dir    

def pathToConfig(state):
    file_name = "config.yml"
    with open(file_name) as f:
        doc = yaml.safe_load(f)
    doc['youtubeDownloader']['copyPath'] = state
    with open(file_name, 'w') as f:
        yaml.safe_dump(doc, f, default_flow_style=False)

def browse_button():
    global filename
    filename = filedialog.askdirectory()
    print(filename)
    o1.delete(0, END)
    o1.insert(0, filename)
    pathToConfig(filename)
    windowSetting.attributes('-topmost', True)
    return filename

def fsettings():
    global o1
    global windowSetting
    windowSetting = Tk()
    windowSetting.title(f"ustawienia")
    windowSetting.geometry('800x620')
    o1label = Label(windowSetting, text="sciezka do zapisu")
    o1 = Entry(windowSetting,width=100)
    o1label.grid(row=0, column=0)
    o1.grid(row=0, column=1)
    o1.insert(0, pathFromConfig())
    settingsButton = Button(windowSetting, text='znajdz', command=browse_button)
    settingsButton.grid(row=0, column=2)
    
def finfoWersion():
    global o1
    global windowSetting
    windowSetting = Tk()
    windowSetting.title(f"informacje o aktualizacjach")
    windowSetting.geometry('800x600')
    
    o1label = Label(windowSetting, text="--- v1.0.7 ---", width=100)    
    o1label.grid(row=11, column=0)
    o1label.config(anchor=CENTER)
    o2label = Label(windowSetting, text="+poprawka bledu sciagania", width=100)    
    o2label.grid(row=10, column=0)
    o2label.config(anchor=CENTER)
    
    o1label = Label(windowSetting, text="--- v1.0.6 ---", width=100)    
    o1label.grid(row=9, column=0)
    o1label.config(anchor=CENTER)
    o2label = Label(windowSetting, text="+krytyczna poprawka naprawijaca blad sciagania", width=100)    
    o2label.grid(row=8, column=0)
    o2label.config(anchor=CENTER)
    
    o1label = Label(windowSetting, text="--- v1.0.5 ---", width=100)    
    o1label.grid(row=7, column=0)
    o1label.config(anchor=CENTER)
    o2label = Label(windowSetting, text="+wylaczenie funkcji domyślnego katalogu do ściągnięcia\n+log błędu braku katalogu docelowego ściągania", width=100)    
    o2label.grid(row=6, column=0)
    o2label.config(anchor=CENTER)
    o1label = Label(windowSetting, text="--- v1.0.4 ---", width=100)    
    o1label.grid(row=5, column=0)
    o1label.config(anchor=CENTER)
    o2label = Label(windowSetting, text="+obsługa Youtube Playlist - można wklejać link Playlisty\n+dodanie funkcji reset (wykorzystać, jeśli program za długo będzie ściagał. Kliknąć reset, następnie ściągnij)", width=100)    
    o2label.grid(row=4, column=0)
    o2label.config(anchor=CENTER)
    o1label = Label(windowSetting, text="--- v1.0.3 ---", width=100)    
    o1label.grid(row=3, column=0)
    o1label.config(anchor=CENTER)
    o2label = Label(windowSetting, text="+usunięcie konsoli, logi w textBoxie", width=100)    
    o2label.grid(row=2, column=0)
    o2label.config(anchor=CENTER)
    o1label = Label(windowSetting, text="--- v1.0.0-v1.0.2 ---", width=100)    
    o1label.grid(row=1, column=0)
    o1label.config(anchor=CENTER)
    o2label = Label(windowSetting, text="+start programu\n+poprawki", width=100)    
    o2label.grid(row=0, column=0)
    o2label.config(anchor=CENTER)

menu = Menu(root)
cascade = Menu(menu)
menu.add_cascade(label="menu", menu = cascade)
cascade.add_command(label = "ustawienia", command=fsettings)
cascade.add_command(label = "info o wersji", command=finfoWersion)
root.config( menu = menu)

# buttons
bpaste = Button(text='wklej', command=bpaste_function)
bpaste.grid(row=2, column=0)
b = Button(text='sciagnij', command=threading)
b.grid(row=2, column=1)
bclear = Button(text='wyczyść okno', command=clear_function)
bclear.grid(row=2, column=2)
breset = Button(text='reset', command=reset_function)
breset.grid(row=2, column=3)

root.mainloop()
