#!/usr/bin/env python
import os
import time
import yaml
import requests, zipfile, io
import os, glob
import shutil
print('hej jestem updater')
class RunUpdater:

    pathExtract = os.getcwd()+'\\TMP'
    pathMainFiles = os.getcwd()+'\\'
    
    def takeTopVersion(self):
        with open('config.yml', 'r') as fp:
            config = yaml.safe_load(fp)
        topVersion = config['youtubeDownloader']['topVersion']
        return topVersion
    
    def downloadZip(self):
        version = self.takeTopVersion()
        topVersionZip = f'http://serverip:8081/{version}/{version}.zip'
        print('top version to', self.takeTopVersion())
        r = requests.get(topVersionZip)
        z = zipfile.ZipFile(io.BytesIO(r.content))
        z.extractall(self.pathExtract)
        
    def deleteMainFiles(self):
        files = [f for f in os.listdir(self.pathMainFiles) if os.path.isfile(os.path.join(self.pathMainFiles, f))]
        dirs = [f for f in os.listdir(self.pathMainFiles) if os.path.isdir(os.path.join(self.pathMainFiles, f))]
        print('files ', files)
        print('dirs ', dirs)
        for file in files:
            if file == 'updater.exe':
                pass
            elif file == 'config.yml':
                pass
            else:
                os.remove(self.pathMainFiles+file)
        for dirElement in dirs:
            if dirElement == 'TMP':
                print('zostawiam TMP')
            else:
                shutil.rmtree(self.pathMainFiles+dirElement)
                
    def copytree(self,src, dst, symlinks=False, ignore=None):
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                if item == 'updater.exe':
                    pass
                elif item == 'config.yml':
                    pass
                else:
                    shutil.copytree(s, d, symlinks, ignore)
            else:
                if item == 'updater.exe':
                    pass
                elif item == 'config.yml':
                    pass
                else:
                    shutil.copy2(s, d)
        
    def deleteTMP(self):
        dirs = [f for f in os.listdir(self.pathMainFiles) if os.path.isdir(os.path.join(self.pathMainFiles, f))]
        for dirElement in dirs:
            if dirElement == 'TMP':
                shutil.rmtree(self.pathMainFiles+dirElement)
            else:
                print('zostawiam bo nie tmp')
        time.sleep(3)
                
if __name__ == '__main__':
    RU = RunUpdater()
    RU.downloadZip()
    RU.deleteMainFiles()
    RU.copytree(RU.pathExtract, RU.pathMainFiles)
    RU.deleteTMP()

os.startfile('start.exe')
