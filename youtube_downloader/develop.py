
#!/usr/bin/python
import shutil
from zipfile import ZipFile
import os
import sys
import shutil
from os.path import basename
import yaml
import subprocess
# developApp = 'pipenv run pyinstaller --noconfirm --onedir --windowed --add-data "E:/!bitbucket/ytdownloader/config.yml;." --add-data "E:/!bitbucket/ytdownloader/requirements.txt;." --add-data "E:/!bitbucket/ytdownloader/youtubelinks.txt;." --add-data "E:/!bitbucket/ytdownloader/updater.py;." start.py'
# developApp = 'pipenv run pyinstaller --noconfirm --onefile --console updater.py'

# os.system(developApp)
with open('config.yml', 'r') as fp:
    config = yaml.safe_load(fp)
    __version__ = config['youtubeDownloader']['version']
shutil.copy('dist/updater.exe', 'dist/start/updater.exe')
shutil.make_archive(__version__, 'zip', 'dist/start/')
server="user@serverip"
destiny=f"/var/www/yt/{__version__}"
src=f'{__version__}.zip'
subprocess.Popen(f"ssh {server} mkdir -p {destiny}", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
subprocess.run(["scp", f'{__version__}.zip', f"{server}:{destiny}/"])



